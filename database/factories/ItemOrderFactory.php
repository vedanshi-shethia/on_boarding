<?php

namespace Database\Factories;

use App\Features\ItemOrder\Domain\Models\ItemOrder;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Products\Domain\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ItemOrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = ItemOrder::class;
    public function definition(): array
    {
        $order = Order::all()->random();
        $product = Product::isActive()->get()->random();
        $quantity = $this->faker->numberBetween(1, 10);
        $unitPrice = $product->price;
        $itemOrder =  [
            'order_id' => $order->id,
            'product_id' => $product->id,
            'quantity' =>$quantity,
            'unit_price' => $unitPrice,
        ];
        $order->net_total += $unitPrice * $quantity;
        $order->save();
        return $itemOrder;
    }
}
