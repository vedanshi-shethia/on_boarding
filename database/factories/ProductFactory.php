<?php

namespace Database\Factories;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    protected  $model = Product::class;

    public function definition(): array
    {
        $category = Category::isActive()->get()->random();
        return [
            'category_id' => $category->id,
            'name' => $this->faker->name(),
            'description' => $this->faker->paragraph(),
            'price' => $this->faker->numberBetween(100, 10000),
            'stock' => $this->faker->numberBetween(1, 10),
            'is_active' => $this->faker->boolean()
        ];
    }
}
