<?php

namespace Database\Factories;

use App\Features\Orders\Domain\Models\Constants\OrderConstants;
use App\Features\Orders\Domain\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Order::class;
    public function definition(): array
    {

        return [
            'customer_name' => $this->faker->name(),
            'customer_email' => $this->faker->unique()->safeEmail(),
            'net_total' => 0,
            'order_date' => $this->faker->date(),
            'order_status' => $this->faker->randomElement([OrderConstants::COMPLETED, OrderConstants::PENDING, OrderConstants::CANCELLED]),
        ];
    }
}
