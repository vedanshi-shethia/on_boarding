<?php

namespace Database\Seeders;

use App\Features\Categories\Domain\Models\Category;
use App\Features\ItemOrder\Domain\Models\ItemOrder;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Products\Domain\Models\Product;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
        ]);

        Category::factory(10)->create();
        Product::factory(50)->create();
        Order::factory(10)->create();
        ItemOrder::factory(40)->create();
    }
}
