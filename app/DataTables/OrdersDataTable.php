<?php

namespace App\DataTables;

use App\Features\Orders\Domain\Models\Constants\OrderConstants;
use App\Features\Orders\Domain\Models\Order;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class OrdersDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->filter(function ($query) {
                if (request()->has('customer_name') && request('customer_name') != null) {
                    $query->where('customer_name','like','%'.request('customer_name').'%');
                }
                if (request()->has('customer_email') && request('customer_email') != null) {
                    $query->where('customer_email','like','%'.request('customer_email').'%');
                }
                if (request()->has('net_total') && request('net_total') != null) {
                    $query->where('net_total',request('net_total'));
                }
                if (request()->has('order_date_from') && request('order_date_from') != null) {
                    $query->whereDate('order_date', '>=', request('order_date_from'));
                }
                if (request()->has('order_date_to') && request('order_date_to') != null) {
                    $query->whereDate('order_date', '<=', request('order_date_to'));
                }
                if (request()->has('created_at_from') && request('created_at_from') != null) {
                    $query->whereDate('created_at', '>=', request('created_at_from'));
                }
                if (request()->has('created_at_to') && request('created_at_to') != null) {
                    $query->whereDate('created_at', '<=', request('created_at_to'));
                }
                if (request()->has('updated_at_from') && request('updated_at_from') != null) {
                    $query->whereDate('updated_at', '>=', request('updated_at_from'));
                }
                if (request()->has('updated_at_to') && request('updated_at_to') != null) {
                    $query->whereDate('updated_at', '<=', request('updated_at_to'));
                }
            })
            ->addColumn("order_status", function ($order) {

                $badgeColor = $order->order_status  === OrderConstants::CANCELLED? "red" : ($order->order_status  === OrderConstants::PENDING? "yellow" : ($order->order_status  === OrderConstants::COMPLETED? "green" : ''));
//                $badgeColor = $order->order_status  === OrderConstants::PENDING? "yellow" : '';
//                $badgeColor = $order->order_status  === OrderConstants::COMPLETED? "green" : '';
                return "<span class = 'bg-{$badgeColor}-lt badge'> {$order->order_status} </span>";
            })
            ->addColumn("action", function ($order) {
                $disableEdit = '';
                $disableStyle = '';
                $route = '"'.route('admin.orders.cancel', $order).'"';
                if($order->order_status !== OrderConstants::PENDING) {
                    $disableEdit = "style='pointer-events: none'";
                    $disableStyle = 'disabled';
                }
                $actions =
                "
                    <a href='" . route("admin.orders.edit", $order->id) . "' ".$disableEdit." class='btn btn-success mb-1 " .$disableStyle."'><i class='ti ti-edit'></i></a>
                    <a href='" . route("admin.orders.show", $order->id) . "' class='btn btn-info'><i class='ti ti-info-circle'></i></a>
                    <a href='" . route("admin.orders.cancel", $order->id) . "' class='btn btn-danger  ".$disableStyle."' data-id = '".$order->id."' ".$disableEdit." data-bs-toggle='modal' data-bs-target='#cancel-model' onclick='setRoute(".$route.")'><i class='ti ti-x'></i></a>
                ";
                return $actions;
            })
            ->addColumn('created_at', function ($order) {
                return $order->created_at->format('Y-m-d');
            })
            ->addColumn('updated_at', function ($order) {
                return $order->updated_at->format('Y-m-d');
            })
            ->orderColumn('order_status', function ($query, $order) {
                $query->orderBy('order_status', $order);
            })
            ->orderColumn('created_at', function ($query, $order) {
                $query->orderBy('created_at', $order);
            })
            ->orderColumn('updated_at', function ($query, $order) {
                $query->orderBy('updated_at', $order);
            })
            ->rawColumns(['action', 'order_status']);
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Order $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('orders-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle();
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('id'),
            Column::make('customer_name'),
            Column::make('customer_email'),
            Column::make('net_total'),
            Column::make('order_date'),
            Column::make('order_status'),
            Column::make('created_at'),
            Column::make('updated_at'),
            [
                'defaultContent' => '',
                'data' => 'action',
                'name' => 'action',
                'title' => 'Action',
                'render' => null,
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => true,
                'footer' => '',
            ]
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'ItemOrders_' . date('YmdHis');
    }
}
