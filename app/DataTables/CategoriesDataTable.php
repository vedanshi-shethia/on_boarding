<?php

namespace App\DataTables;
use App\Features\Categories\Domain\Models\Category;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\WithExportQueue;

class CategoriesDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    use WithExportQueue;
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn("is_active", function ($category) {
                $badgeColor = $category->is_active ? "green" : "red";
                $text = $category->is_active ? "Active": "Inactive";
                return "<span class = 'badge bg-{$badgeColor}-lt'> {$text} </span>";
            })
            ->addColumn("action", function ($category) {
                $route = '"'.route('admin.categories.destroy', $category).'"';
                $deleteAction = "
                        <button class='btn btn-danger' data-bs-toggle='modal' data-bs-target='#delete-model' onclick='setRoute(".$route.")'><i class='ti ti-trash'></i></button>
                ";
                $editAction = "<a href='" . route("admin.categories.edit", $category->id) . "' class='btn btn-success'><i class='ti ti-edit'></i></a>";
                return $editAction.$deleteAction;
            })
            ->addColumn('description', function ($product) {
                return substr($product->description, 0, 50) . '....';
            })
            ->addColumn('created_at', function ($category) {
                return $category->created_at->format('Y-m-d');
            })
            ->addColumn('updated_at', function ($category) {
                return $category->updated_at->format('Y-m-d');
            })
            ->rawColumns(['action', 'is_active'])
            ->filter(function ($query) {
                if (request()->has('is_active') && request('is_active') != null) {
                    $query->where('is_active',request('is_active'));
                }
            })
            ->orderColumn('description', function ($query, $product) {
                $query->orderBy('description', $product);
            })
            ->orderColumn('is_active', function ($query, $category) {
                $query->orderBy('is_active', $category);
            })
            ->orderColumn('created_at', function ($query, $category) {
                $query->orderBy('created_at', $category);
            })
            ->orderColumn('updated_at', function ($query, $category) {
                $query->orderBy('updated_at', $category);
            });
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Category $model): QueryBuilder
    {
        $query = $model->newQuery();
        return $query;
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('categories-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
//                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle();
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('id'),
            Column::make('name'),
            Column::make('description'),
            Column::computed('is_active'),
            Column::make('created_at'),
            Column::make('updated_at'),
            [
                'defaultContent' => '',
                'data'           => 'action',
                'name'           => 'action',
                'title'          => 'Action',
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ]
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Categories_' . date('YmdHis');
    }
}
