<?php

namespace App\DataTables;

use App\Features\Products\Domain\Models\Product;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ProductsDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->filter(function ($query) {
//                Log::error($this->request->toArray());
//                Log::info( ($this->request->category_id == null));
                if (request()->has('category_id') && request('category_id') != null) {
                    $query->where('products.category_id',request('category_id'));
                }
                if (request()->has('name') && request('name') != null) {
                    $query->where('products.name','like','%'.request('name').'%');
                }
                if (request()->has('description') && request('description') != null) {
                    $query->where('products.description', 'like', '%'.request('description').'%');
                }
                if (request()->has('is_active') && request('is_active') != null) {
                    $query->where('products.is_active',request('is_active'));
                }
                if (request()->has('price') && request('price') != null) {
                    $query->where('products.price',request('price'));
                }
                if (request()->has('stock') && request('stock') != null) {
                    $query->where('products.stock',request('stock'));
                }
                if (request()->has('created_at_from') && request('created_at_from') != null) {
                    $query->whereDate('products.created_at', '>=', request('created_at_from'));
                }
                if (request()->has('created_at_to') && request('created_at_to') != null) {
                    $query->whereDate('products.created_at', '<=', request('created_at_to'));
                }
                if (request()->has('updated_at_from') && request('updated_at_from') != null) {
                    $query->whereDate('products.updated_at', '>=', request('updated_at_from'));
                }
                if (request()->has('updated_at_to') && request('updated_at_to') != null) {
                    $query->whereDate('products.updated_at', '<=', request('updated_at_to'));
                }
            })
            ->addColumn('category_id', function (Product $product) {
                return $product->category_name;
            })
            ->addColumn("is_active", function ($product) {
                $badgeColor = $product->is_active ? "green" : "red";
                $text = $product->is_active ? "Active" : "Inactive";
                return "<span class = 'bg-{$badgeColor}-lt badge '> {$text} </span>";
            })
            ->addColumn("action", function ($product) {
                $route = '"'.route('admin.products.destroy', $product).'"';
                $editAction = "<a href='" . route("admin.products.edit", $product->id) . "' class='btn btn-success'><i class='ti ti-edit'></i></a>";

                $deleteAction = " <button class='btn btn-danger' data-bs-toggle='modal' data-bs-target='#delete-model' onclick='setRoute(".$route.")'><i class='ti ti-trash'></button>";

                return $editAction . $deleteAction;
            })
            ->addColumn('description', function ($product) {
                return Str::limit($product->description, 50);
            })
            ->addColumn('created_at', function ($product) {
                return $product->created_at->format('Y-m-d');
            })
            ->addColumn('updated_at', function ($product) {
                return $product->updated_at->format('Y-m-d');
            })
            ->rawColumns(['action', 'is_active'])
            ->orderColumn('description', function ($query, $product) {
                $query->orderBy('description', $product);
            })
            ->orderColumn('category_id', function ($query, $order) {
                Log::info($order);
                $query->orderBy('categories.name', $order);
            })
            ->orderColumn('is_active', function ($query, $order) {
                $query->orderBy('is_active', $order);
            })
            ->orderColumn('created_at', function ($query, $order) {
                $query->orderBy('created_at', $order);
            })
            ->orderColumn('updated_at', function ($query, $order) {
                $query->orderBy('updated_at', $order);
            });
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Product $model): QueryBuilder
    {
//        dd($model->with('category')->newQuery()->toSql());
//        return Product::join('categories', 'products.category_id', '=', 'categories.id')->select('products.*', 'categories.name as category_name')->orderBy('categories.name')->newQuery();
        return Product::join('categories', 'products.category_id', '=', 'categories.id')->select('products.*', 'categories.name as category_name')->newQuery();
//        return Product::with('category:id,name')->newQuery();
//        return $model->with(['category' => function($query) {
//            \Log::info("Inside Sub Query!");
//            $query->orderBy('categories.name', 'asc');
//        }])->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('products-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            //->dom('Bfrtip')
            ->orderBy(1)
            ->selectStyleSingle();
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('id'),
            Column::make('category'),
            Column::make('name'),
            Column::make('description'),
            Column::make('price'),
            Column::computed('stock'),
            Column::computed('is_active'),
            Column::make('created_at'),
            Column::make('updated_at'),
            [
                'defaultContent' => '',
                'data' => 'action',
                'name' => 'action',
                'title' => 'Action',
                'render' => null,
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => true,
                'footer' => '',
            ]
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Products_' . date('YmdHis');
    }
}
