<?php

namespace App\Providers;

use App\Features\Orders\Domain\Models\Order;
use App\Policies\OrderPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    private const DATABASE_MIGRATION = [
        __DIR__.'/../Features/Categories/Migrations',
        __DIR__.'/../Features/Products/Migrations',
        __DIR__.'/../Features/Orders/Migrations',
        __DIR__.'/../Features/ItemOrder/Migrations',
    ];
    public function register(): void
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Debugbar', \Barryvdh\Debugbar\Facades\Debugbar::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(self::DATABASE_MIGRATION);
        Gate::policy(Order::class, OrderPolicy::class);
    }
}
