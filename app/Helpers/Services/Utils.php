<?php

namespace App\Helpers\Services;

use App\Features\Products\Domain\Models\Exceptions\IsNotActiveException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class Utils
{
    public static function validateOrThrow(
        array $data,
        array $validationRules,
        array $validationMessages = [],
    ): array {
//        dd("inside");
        $validator = Validator::make($data, $validationRules, $validationMessages);
//        dd($validationRules);
        Log::error($validator->getMessageBag());
        if($validator->fails()) {
            throw ValidationException::withMessages($validator->getMessageBag()->toArray());
        }

        return $validator->validated();
    }

    public static function isActiveValidateOrThrow(
        Model $parentData,
        array $data,
    )
    {
//        dd($parentData);

        if($data != null) {
            if($parentData->is_active) {
                return true;
            }else {
                throw new IsNotActiveException("Is not active" . $parentData);
            }
        }

    }

}
