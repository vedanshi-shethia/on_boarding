<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductsHeaderExport implements  WithHeadings, ShouldAutoSize
{

    public function headings():array
    {
        return [
            "Category Name",
            "Product Name",
            "Product Description",
            "Product Status",
            "Product Price",
            "Product Stock",
            "Product Status"
        ];
    }
}
