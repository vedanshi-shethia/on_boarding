<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SkeletonExport implements WithMultipleSheets, WithStyles
{
    public function sheets(): array{
        $sheets = [
            new CategoriesHeaderExport(),

        ];
        return [
            new CategoriesHeaderExport(),
            new ProductsHeaderExport()
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
