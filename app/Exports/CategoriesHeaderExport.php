<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CategoriesHeaderExport implements  WithHeadings, ShouldAutoSize
{

    public function headings(): array
    {
        return [
            "Name",
            "Description",
            "Status"
        ];
    }
}
