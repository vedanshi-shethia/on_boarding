<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class ProductExportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "category_id" => "exists:categories,id|accepted:true",
            "name" => "string|max:255",
            "description" => "nullable|string",
            "price" => "numeric|min:0",
            "stock" => "numeric|min:0",
            "is_active" => "boolean",
        ];
    }
}
