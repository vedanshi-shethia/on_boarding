<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "product" => "required|array",
            "product.*.category_id" => "required|exists:categories,id",
            "product.*.name" => "required|string|max:255",
            "product.*.description" => "nullable|string",
            "product.*.price" => "required|numeric|min:0",
            "product.*.stock" => "required|numeric|min:0",
            "product.*.is_active" => "required|boolean",
        ];
    }
}
