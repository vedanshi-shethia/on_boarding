<?php

namespace App\Http\Requests\Orders;

use App\Features\Products\Domain\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
//        dd(request());
        return [
            "order"=>"required|array",
            "order.customer_name" => "required",
            "order.customer_email" => "required|email",
            "order.order_date" => "required|date",
            "item_order" => "required|array",
            "item_order.*.category_id" => "required|exists:categories,id",
            "item_order.*.product_id" => "required|exists:products,id",
            "item_order.*.quantity" => [
                "numeric",
                "required",
                "min:1",
                function ($attribute, $value, $fail) {
                    $index = explode('.', $attribute)[1];
                    $productId = request()->item_order[$index]['product_id'];
                    $productStock = Product::where('id', $productId)->firstOrFail()->stock;
                    $productStock === 0 ? $fail('Product stock is 0 ') : null;
                    $value > $productStock ? $fail("Quantity should not be greater than Stock") : null;
                }
            ],
        ];
    }
}
