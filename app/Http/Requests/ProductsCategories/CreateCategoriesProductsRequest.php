<?php

namespace App\Http\Requests\ProductsCategories;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoriesProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
//        dd(request());

        return [
            "category.name" => "required|string|max:255",
            "category.description" => "nullable|string",
            "category.is_active" => "required|boolean",
            "product" => "array",
            "product.*.name" => "required|string|max:255",
            "product.*.description" => "nullable|string",
            "product.*.price" => "required|numeric",
            "product.*.stock" => "required|numeric",
            "product.*.is_active" => "required|boolean",
        ];
    }
}
