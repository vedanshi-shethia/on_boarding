<?php

namespace App\Http\Middleware;

use App\Features\Orders\Domain\Models\Constants\OrderConstants;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UpdatingValidOrders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        //        dd($request);
        $order = $request->route('order');
//        dd($order);
        if(!($order->order_status === OrderConstants::PENDING)) {
            abort(403, "Order Status Must be Pending");
        }
        return $next($request);
    }
}
