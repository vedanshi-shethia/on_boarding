<?php

namespace App\Features\ItemOrder\Http\Controllers\Admin\V1\Actions;

use App\Features\ItemOrder\Domain\Models\ItemOrder;
use App\Features\Orders\Domain\Models\Order;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class ItemOrdersAction
{
    public function updateItemOrders(array $data, Order $order, Collection $itemOrders) {
        $netTotal = $order->net_total;
        Log::error(["Net Total : " => $netTotal]);
        foreach ($data['item_order'] as $requestItemOrder) {
            Log::error($requestItemOrder);
            if($requestItemOrder['old_quantity']){
                if($requestItemOrder['old_quantity'] === $requestItemOrder['quantity']){
                    $itemOrder = $itemOrders->where('order_id', $order->id)->where('product_id',  $requestItemOrder['product_id'])->where('quantity', $requestItemOrder['old_quantity'])->first();
                    $itemOrders = $itemOrders->reject(fn ($value) => $value == $itemOrder);
                    Log::error("Removing from array");
                    Log::error($itemOrder);
                    Log::error($itemOrders);
                }else {
                    $itemOrder = $itemOrders->where('order_id', $order->id)->where('product_id',  $requestItemOrder['product_id'])->first();
                    $netTotal += $itemOrder->updateItemOrder($requestItemOrder, $order);
                    Log::error(["Update Net Total : " => $netTotal]);
                    $itemOrders = $itemOrders->reject(fn ($value) => $value == $itemOrder);
                }
            }else {
                Log::error("Creating..");
                Log::error($requestItemOrder);
                $netTotal += ItemOrder::persistItemOrder($requestItemOrder, $order);
                Log::error(["Create Net Total : " => $netTotal]);
            }
        }

        foreach($itemOrders as $itemOrder){
            $netTotal -= ($itemOrder->quantity * $itemOrder->unit_price);
            Log::error("Deleting");
            Log::error($itemOrder);
            Log::error(["Delete Net Total : " => $netTotal]);
            $itemOrder->deleteItemOrders();
        }

        $order->net_total = $netTotal;
        Log::error(["End Net Total : " => $netTotal]);
        $order->save();
    }
}
