<?php

namespace App\Features\ItemOrder\Domain\Models\Constants;

interface ItemOrderConstants
{
    const CREATE_VALIDATION_RULES = [
        "category_id" => "required|exists:categories,id",
        "product_id" => "required|exists:products,id",
    ];

    const UPDATE_VALIDATION_RULES = [
        "category_id" => "required|exists:categories,id",
        "product_id" => "required|exists:products,id",
    ];
}
