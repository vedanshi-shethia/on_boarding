<?php

namespace App\Features\ItemOrder\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use App\Features\ItemOrder\Domain\Models\Constants\ItemOrderConstants;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Products\Domain\Models\Product;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Database\Factories\ItemOrderFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class ItemOrder extends BaseModel implements ItemOrderConstants
{
    protected $table = 'item_order';
    protected static function newFactory()
    {
        return ItemOrderFactory::new();
    }
    public static function persistItemOrder(array $data, Order $order) : int{

        $category =  Category::where('id', $data['category_id'])->first();
        $product = Product::where('id', $data['product_id'])->first();
        Utils::isActiveValidateOrThrow($category, $data);
        Utils::isActiveValidateOrThrow($product, $data);
        Utils::validateOrThrow($data, self::CREATE_VALIDATION_RULES);

        return DB::transaction(function () use($data, $order, $product) {

            $product_id = ['product_id' => $product->id];
            if($data['quantity'] > $product->stock) {
                throw ValidationException::withMessages("Quanity should not be greater than stock");
            }
            $itemOrder['quantity'] = $data['quantity'];
            $itemOrder['unit_price'] = $product->price;
            $product->update(['stock' => ($product->stock - $itemOrder['quantity'])]);
            Log::info($product->stock);
            $order->products()->attach($product_id, $itemOrder);
            return $product->price * $data['quantity'];
        });
    }

    public function updateItemOrder(array $data, Order $order){
        $category =  Category::where('id', $data['category_id'])->first();
        $product = Product::where('id', $data['product_id'])->first();
        Utils::isActiveValidateOrThrow($category, $data);
        Utils::isActiveValidateOrThrow($product, $data);
        Utils::validateOrThrow($data, self::UPDATE_VALIDATION_RULES);

        return DB::transaction(function () use($data, $order, $product) {
            $value = $data['old_quantity'] - $data['quantity'];
            $this->update(['quantity' => $data['quantity']]);
            $product->update(['stock' => $product->stock + $value]);
            return (($data['quantity'] - $data['old_quantity']) * $product->price);
        });
    }

    public function deleteItemOrders(){
        $product = Product::where('id',$this->product_id)->first();
        $product->update(['stock' => $product->stock + $this->quantity]);
        $this->delete();
    }
}
