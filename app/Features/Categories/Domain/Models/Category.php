<?php

namespace App\Features\Categories\Domain\Models;

use App\Features\Categories\Domain\Models\Constants\CategoryConstants;
use App\Features\Products\Domain\Models\Product;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Database\Factories\CategoryFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class Category extends BaseModel implements CategoryConstants
{
    public function products() : HasMany
    {
        return $this->hasMany(Product::class, "category_id");
    }
    protected static function newFactory()
    {
        return CategoryFactory::new();
    }

    public static function persistCategory(array $data) : self
    {
        Utils::validateOrThrow($data, self::CREATE_VALIDATION_RULES);
        return DB::transaction(fn () => Category::create($data) );
    }

    public function updateCategory(array $data) : self{
        Utils::validateOrThrow($data, self::UPDATE_VALIDATION_RULES);
        DB::transaction(fn () => $this->update($data));
        return $this;
    }

    public function deleteCategory() : bool{
        if(count($this->products()->get()) === 0) {
            DB::transaction(fn () => $this->delete() );
            return true;
        }else {
            throw ValidationException::withMessages(['error' => 'Category has products']);
        }
    }

    public function scopeIsActive($query) {
        return $query->where('is_active', true);
    }

    public static function getActiveCategories() {
        return Category::isActive()->get();
    }

    public static function getById(int $id) : Category{
        return Category::where('id', $id)->firstOrfail();
    }

    public function iSActiveValidation() {
        Utils::validateOrThrow($this->toArray(), [
            "is_active" => function ($attribute, $value, $fail) {
                Log::error($value);
                $value ? null : $fail('Category is not active so products cannot be created');
            }
        ]);
    }
}
