<?php

namespace App\Features\Categories\Domain\Models\Constants;

interface CategoryConstants
{

    public const CREATE_VALIDATION_RULES = [
        "name" => "required|string|max:255",
        "description" => "nullable|string",
        "is_active" => "required|boolean",
    ];
    public const UPDATE_VALIDATION_RULES = [
        "name" => "required|string|max:255",
        "description" => "nullable|string",
        "is_active" => "required|boolean",
    ];
}
