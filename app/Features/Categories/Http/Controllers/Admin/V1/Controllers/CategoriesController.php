<?php

namespace App\Features\Categories\Http\Controllers\Admin\V1\Controllers;

use App\DataTables\CategoriesDataTable;
use App\Features\Categories\Domain\Models\Category;
use App\Features\Categories\Http\Controllers\Admin\V1\Actions\CategoriesAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\CreateCategoryRequest;
use App\Http\Requests\Categories\UpdateCategoriesRequest;
use Illuminate\Support\Facades\Log;

class CategoriesController extends Controller
{
    public function __construct(private CategoriesAction $categoriesAction) {}

    public function index(CategoriesDataTable $dataTable) {
        return $dataTable->render('admin.categories.index');
    }

    public function create(){
        return view('admin.categories.create');
    }

    public function store(CreateCategoryRequest $request){
        try{
            $data["name"] = $request->name;
            $data["description"] = $request->description;
            $data["is_active"] =$request->is_active === "1";
            $this->categoriesAction->persistCategory($data);
            session()->flash('success', 'Category created successfully');

        }catch(\Exception $exception) {
            Log::error($exception->getMessage());
            session()->flash('error', 'Category created failed');
        }

        return redirect()->route('admin.categories.index');
    }

    public function edit(Category $category){
        return view('admin.categories.edit', compact(['category']));
    }

    public function update(UpdateCategoriesRequest $request, Category $category)
    {
        try{
            $data["name"] = $request->name;
            $data["description"] = $request->description;
            $data["is_active"] =$request->is_active === "1";
            $this->categoriesAction->updateCategory($category, $data);
            session()->flash('success', 'Category Updated successfully');

        }catch(\Exception $exception) {
            Log::error($exception->getMessage());
            session()->flash('error', 'Failed Category Update');
        }

        return redirect()->route('admin.categories.index');
    }

    public function destroy(Category $category){
        try{
            $this->categoriesAction->deleteCategory($category);
            session()->flash('success', 'Category deleted successfully');
        }
        catch(\Exception $exception){
            Log::error($exception->getMessage());
            session()->flash('error', $exception->getMessage());
        }
        return redirect()->route('admin.categories.index');
    }
}
