<?php

namespace App\Features\Categories\Http\Controllers\Admin\V1\Actions;

use App\Features\Categories\Domain\Models\Category;

class CategoriesAction
{
    public function persistCategory(array $data) : Category
    {
        return Category::persistCategory($data);
    }

    public function updateCategory(Category $category, array $data) : Category
    {
        return $category->updateCategory($data);
    }
    public function deleteCategory(Category $category) : bool{
        return $category->deleteCategory();
    }
}
