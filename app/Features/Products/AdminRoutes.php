<?php

use Illuminate\Support\Facades\Route;

Route::resource("/products",\App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsController::class)->except('show');

Route::get("/categories/products/create", [\App\Features\Products\Http\Controllers\Admin\V1\Controllers\CategoriesProductsController::class, "create"])->name("categories.products.create");

Route::post("/categories/products", [\App\Features\Products\Http\Controllers\Admin\V1\Controllers\CategoriesProductsController::class, "store"])->name("categories.products.store");

Route::post("/products/import", [\App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsImportController::class, "import"])->name("products.import");

Route::post("/products/errorsExport", [\App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsImportController::class, "errorsExport"])->name("products.errorsExport");

Route::post("products/export", [\App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsExportController::class, "export"])->name('products.export');

Route::post("products/exportSkeleton", [\App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsExportController::class, 'exportSkeleton'])->name('products.exportSkeleton');

