<?php

namespace App\Features\Products\Http\Controllers\Admin\V1\Controllers;

use App\DataTables\ProductsDataTable;
use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Constants\ProductConstants;
use App\Features\Products\Domain\Models\Product;
use App\Features\Products\Http\Controllers\Admin\V1\Actions\ProductsAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Products\CreateProductRequest;
use App\Http\Requests\Products\UpdateProductRequest;
use Illuminate\Support\Facades\Log;

class ProductsController extends Controller
{
    public function __construct(private ProductsAction $productsAction){}
    public function index(ProductsDataTable $dataTable){
        $categories = $this->productsAction->getAllCategories();
        return $dataTable->render('admin.products.index', compact(['categories']));
    }

    public function create(){
        $categories = Category::all();
        return view('admin.products.create', compact(['categories']));
    }

    public function store(CreateProductRequest $request){
        try{
            $data = $request->product;
            $this->productsAction->presistProducts($data);
            session()->flash('success', 'Product created successfully');
        }catch(\Exception $e){
            Log::error($e->getMessage());
            session()->flash('error', 'Something went wrong!');
        }
        return redirect()->route('admin.products.index');
    }

    public function edit(Product $product){
        $categories = $this->productsAction->getAllCategories();
        return view('admin.products.edit', compact(['product', 'categories']));
    }

    public function update(UpdateProductRequest $request, Product $product){
        try{
            $data["category_id"] = $request['category_id'];
            $data["name"] = $request["name"];
            $data["description"] = $request["description"];
            $data["price"] = $request["price"];
            $data["stock"] = $request["stock"];
            $data["is_active"] = $request["is_active"] === ProductConstants::ACTIVE;
            $this->productsAction->updateProduct($product, $data);
            session()->flash('success', 'Product updated successfully');
        }catch(\Exception $e){
            Log::error($e->getMessage());
            session()->flash('error', 'Something went wrong!');
        }
        return redirect()->route('admin.products.index');
    }

    public function destroy(Product $product)
    {
        try{
            $this->productsAction->deleteProduct($product);
            session()->flash('success', 'Product deleted successfully');
        }catch(\Exception $e){
            Log::error($e->getMessage());
            session()->flash('error', $e->getMessage());
        }
        return redirect()->route('admin.products.index');
    }

}
