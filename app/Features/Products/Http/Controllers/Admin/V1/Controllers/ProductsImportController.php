<?php

namespace App\Features\Products\Http\Controllers\Admin\V1\Controllers;

use App\Features\Products\Domain\Jobs\ProductsImportJob;
use App\Features\Products\Http\Controllers\Admin\V1\Actions\ProductsImportsAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Products\ProductsImportRequest;
use Illuminate\Support\Facades\Log;

class ProductsImportController extends Controller
{

    public function __construct(private ProductsImportsAction $productsImportsAction) {
    }
    public function import(ProductsImportRequest $request) {
        try {
            $file = request()->file('products');
            $storedFile = $file->storeAs(
                "temp/import-files",
                "temp".uniqid().".".$file->getClientOriginalName()
            );
            $this->productsImportsAction->importProducts($storedFile);
            session()->flash('success', 'Products import started successfully');
            return redirect()->back();
        }catch(\Exception $exception) {
            Log::error("$exception");
        }catch (\Throwable $exception) {
            Log::error("$exception");
        }
        return redirect()->back();
    }
}
