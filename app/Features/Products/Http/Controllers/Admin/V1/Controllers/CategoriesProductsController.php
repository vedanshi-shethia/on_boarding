<?php

namespace App\Features\Products\Http\Controllers\Admin\V1\Controllers;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Http\Controllers\Admin\V1\Actions\CategoriesProductsAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductsCategories\CreateCategoriesProductsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CategoriesProductsController extends Controller
{
    public function __construct(private CategoriesProductsAction $categoriesProductsAction)
    {

    }
    public function create() {
        $categories = $this->categoriesProductsAction->getCategories();
        return view('admin.categories.products.create', compact(['categories']));
    }

    public function store(CreateCategoriesProductsRequest $request) {
        try{
            $category = $request->category;
            $product = $request->product != null ? $request->product: [];

            $this->categoriesProductsAction->persistCategoriesProducts($category, $product);
            session()->flash('success', "Product Created successfully");
        }catch (\Exception $exception){
            Log::error($exception);
            session()->flash('error', $exception->getMessage());
        }

        return redirect()->route('admin.products.index');
    }
}
