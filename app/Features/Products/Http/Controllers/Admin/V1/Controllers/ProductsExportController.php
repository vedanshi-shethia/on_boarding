<?php

namespace App\Features\Products\Http\Controllers\Admin\V1\Controllers;

use App\Exports\SkeletonExport;
use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Exports\ProductsExport;
use App\Features\Products\Domain\Models\Product;
use App\Features\Products\Http\Controllers\Admin\V1\Actions\ProductsExportAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Products\ProductExportRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ProductsExportController extends Controller
{
    public function __construct(private ProductsExportAction $productsExportAction) {}

    public function export(ProductExportRequest $request)
    {
//        $data = [];
//        $request->category ? $data['category_id'] = $request->category_id: null;
//        $request->name ? $data['name'] = $request->name : null;
//        $request->description ? $data['description'] = $request->description : null;
//        $request->is_active ? $data['is_active'] = $request->is_active : null;
//        $request->price ? $data['price'] = $request->price : null;
//        $request->stock ? $data['stock'] = $request->stock : null;
//        $request->created_at ? $data['created_at'] = $request->creadted_at : null;
        $data = $request->all();
        Log::info($data);
       return $this->productsExportAction->exportProducts($data);
    }

    public function exportSkeleton() {
        return $this->productsExportAction->exportSkeleton();
    }




}
