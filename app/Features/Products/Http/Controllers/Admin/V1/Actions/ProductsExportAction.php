<?php

namespace App\Features\Products\Http\Controllers\Admin\V1\Actions;

use App\Exports\SkeletonExport;
use App\Features\Products\Domain\Exports\ProductsExport;
use App\Features\Products\Domain\Models\Product;
use Maatwebsite\Excel\Facades\Excel;

class ProductsExportAction
{

    public function exportProducts($data) {
        $query = Product::query()->with('category');
        foreach($data as $attribute=> $value) {
            $query = $query->where($attribute,'like','%'. $value.'%');
        }
        $products = $query->get();
        return Excel::download(new ProductsExport($products), 'products.xlsx');
    }

    public function exportSkeleton() {
        return Excel::download(new SkeletonExport(), 'importFileFormat.xlsx');
    }

}
