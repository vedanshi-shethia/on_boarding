<?php

namespace App\Features\Products\Http\Controllers\Admin\V1\Actions;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Product;

class ProductsAction
{
    public function presistProducts(array $data) {
        foreach($data as $product) {
            Product::persistProduct($product);
        }
    }

    public function getCategory($name) : int{
//        dd(Category::where('name', $name)->get()->first());
        return Category::where('name', $name)->get()->first()->id;
    }
    public function updateProduct(Product $product,  $data) :Product
    {
        return $product->updateProduct($data);
    }

    public function deleteProduct(Product $product) {
        $product->deleteProduct();
    }

    public function getAllActiveCategories() {
        return Category::isActive()->get();
    }

    public function  getAllCategories() {
        return Category::all();
    }

}
