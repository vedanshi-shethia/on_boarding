<?php

namespace App\Features\Products\Http\Controllers\Admin\V1\Actions;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Product;
use App\Helpers\Services\Utils;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CategoriesProductsAction
{
    public function persistCategoriesProducts(array $categoryData, array $productData) : Collection{
//        return DB::transaction(function () use($categoryData, $productData) {
            $category = Category::persistCategory($categoryData);
            return Product::persistManyProducts($productData, $category);
//        });
    }
    public function getCategories() {
        return Category::getActiveCategories();
    }
}
