<?php

namespace App\Features\Products\Http\Controllers\Admin\V1\Actions;

use App\Features\Products\Domain\Jobs\ProductsImportJob;

class ProductsImportsAction
{

    public function importProducts($storedFile) {
//        dd("inside");
        ProductsImportJob::dispatch($storedFile);
    }

}
