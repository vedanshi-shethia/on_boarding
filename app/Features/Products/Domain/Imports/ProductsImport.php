<?php

namespace App\Features\Products\Domain\Imports;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Exports\FailureReportExport;
use App\Features\Products\Domain\Jobs\FailureReportMailJob;
use App\Features\Products\Domain\Models\Constants\ProductConstants;
use App\Features\Products\Domain\Models\Product;
use App\Mail\FailureReportMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Collection as DBCollection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Facades\Excel;

class ProductsImport implements ToCollection, WithHeadingRow
{
    use Importable;
    const ATTRIBUTES = [
        "category_id",
        "name",
        "description",
        "price",
        "stock",
        "is_active",
    ];

    private DBCollection $errorBag;
    public function __construct(private DBCollection $categories){
        $this->errorBag = new DBCollection();
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $productData = $this->getProductData($row);
            $exportRows = [];
            $rules = [
                "category_id" => [
                    "required",
                    "numeric",
                    "exists:categories,id",
                    function ($attribute, $value, $fail) use($productData) {
                        $category = $this->categories->where('id', $productData['category_id'])->first();
                        $category ? null : $fail('Category is not active so products cannot be created');
                    }],
                "name" => "required|string|max:255",
                "description" => "nullable|string",
                "price" => "required|numeric|min:0",
                "stock" => "required|numeric|min:0",
                "is_active" => "required|boolean",
            ];
            $validator = Validator::make($productData, $rules);


            foreach (self::ATTRIBUTES as $attribute) {
                if($attribute === "is_active") {
                    Log::info($productData[$attribute] ? "Active" : "Inactive");
                    $exportRows[$attribute] = $productData[$attribute] ? "Active" : "Inactive";
                } else {
                    $exportRows[$attribute] = $productData[$attribute];
                }
            }
            if ($validator->fails()) {
                $exportRows["errors"] = '';
                foreach (array_keys($validator->errors()->getMessages()) as $attribute) {
                    Log::info($validator->errors()->getMessages()[$attribute]);
                    $exportRows["errors"] .= (implode(',', $validator->errors()->getMessages()[$attribute]) . ',');
                }
            }else {
                Product::create($productData);
            }
            $this->errorBag->push($exportRows);
        }
        if($this->errorBag) {
            Excel::store(new FailureReportExport($this->errorBag), 'errors.csv', 'public');
            FailureReportMailJob::dispatch();
        }
    }

    public function getProductData(Collection $row) : array {
            return [
                "category_id" => $row["category_id"],
                "name" => $row["product_name"],
                "description" => $row["product_description"],
                "price" => $row["product_price"],
                "stock" => $row["product_stock"],
                "is_active" => $row["product_status"] == "Active"
            ];
    }
}
