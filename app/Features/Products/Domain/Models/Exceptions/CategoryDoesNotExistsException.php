<?php

namespace App\Features\Products\Domain\Models\Exceptions;

class CategoryDoesNotExistsException extends \Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }

}
