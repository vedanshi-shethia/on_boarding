<?php

namespace App\Features\Products\Domain\Models\Exceptions;

class IsNotActiveException extends \Exception
{
//    protected $message = "The category is not active";

    public function __construct(string $message = ""){
        parent::__construct($message);
    }
}
