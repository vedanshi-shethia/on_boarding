<?php

namespace App\Features\Products\Domain\Models\Constants;

interface ProductConstants
{

    public const ACTIVE = "1";
    public const INACTIVE = "0";
    public const CREATE_VALIDATION_RULES = [
        "name" => "required|string|max:255",
        "description" => "nullable|string",
        "price" => "required|numeric|min:0",
        "stock" => "required|numeric|min:0",
        "is_active" => "required|boolean",
    ];

    public const CREATE_MANY_VALIDATION_RULES = [
        "*.name" => "required|string|max:255",
        "*.description" => "nullable|string",
        "*.price" => "required|numeric",
        "*.stock" => "required|numeric",
        "*.is_active" => "required|boolean",
    ];

    public const UPDATE_VALIDATION_RULES = [
        "category_id" => "exists:categories,id",
        "name" => "required|string|max:255",
        "description" => "nullable|string",
        "price" => "required|numeric",
        "stock" => "required|numeric",
        "is_active" => "required|boolean",
    ];
}
