<?php

namespace App\Features\Products\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Constants\ProductConstants;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Database\Factories\ProductFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use \Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends BaseModel implements ProductConstants
{
    public function category() : BelongsTo{
        return $this->belongsTo(Category::class, "id", "category_id");
    }

    public static function persistProduct(array $data): self {
//        dd($data);
        $category = Category::where('name', $data['category'])->get()->first();
        Utils::isActiveValidateOrThrow($category, $data);
        Utils::validateOrThrow($data, self::CREATE_VALIDATION_RULES);
        return DB::transaction(function () use ($category, $data) {
            return $category->products()->create($data);
        });
    }

    public function updateProduct(array $data): self {
//        dd($data);
        $category = Category::where('id', $data['category_id'])->get()->first();
//        dd($category);
        Utils::isActiveValidateOrThrow($category, $data);
        Utils::validateOrThrow($data, self::UPDATE_VALIDATION_RULES);
//        dd("passed validation");
        DB::transaction(function () use ($data) {
            $this->update($data);
        });
        return $this;
    }

    public function deleteProduct() {
        DB::transaction(function () {
            $this->delete();
        });
    }

    public function scopeIsActive($query) {
        return $query->where('is_active', true);
    }

    protected static function newFactory()
    {
        return ProductFactory::new();
    }
}
