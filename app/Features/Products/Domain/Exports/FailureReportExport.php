<?php

namespace App\Features\Products\Domain\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Database\Eloquent\Collection;

class FailureReportExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    use Exportable;

    /**
     * @return \Illuminate\Support\Collection
     */
    private $failures;

    public function __construct(Collection $failures)
    {
        $this->failures = $failures;
//        Log::error("inside export");
//        Log::error($failures);
    }

    public function collection()
    {
        return $this->failures;
    }


    public function headings(): array
    {
        return [
            "Category Id",
            "Name",
            "Description",
            "Price",
            "stock",
            "Status",
            "Errors",
        ];
    }

}
