<?php

namespace App\Features\Products\Domain\Exports;

use App\Features\Categories\Domain\Models\Category;
use Maatwebsite\Excel\Concerns\FromCollection;
use  Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ProductsExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private Collection $products;

    public function __construct(Collection $products)
    {
        $this->products = $products;
    }
    public function collection()
    {
        $productData = new Collection();
        foreach ($this->products as $product) {
            $data['Id'] = $product['id'];
//            dd($product->category)
            $data['category'] = $product->category->name;
            $data['name'] = $product['name'];
            $data['description'] = $product['description'];
            $data['price'] = $product['price'];
            $data['stock'] = $product['stock'];
            $data['is_active'] = $product['is_active'] ? 'Active' : 'Inactive';
            $data['created_at'] = $product['created_at']->diffForHumans();
            $data['updated_at'] = $product['updated_at']->diffForHumans();
            $productData->push($data);
        }
//        dd($this->products);
        return $productData;
    }

    public function headings() :array
    {
        return [
            "Id",
            "Product Category Name",
            "Product Name",
            "Product Description",
            "Product Price",
            "Product Stock",
            "Product Status",
            "Product Created At",
            "Product Updated At",
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
