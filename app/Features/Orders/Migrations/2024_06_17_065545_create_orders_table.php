<?php

use App\Features\Orders\Domain\Models\Constants\OrderConstants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('customer_name');
            $table->string('customer_email');
            $table->unsignedInteger('net_total')->nullable();
            $table->dateTime('order_date');
            $table->set('order_status', [OrderConstants::PENDING, OrderConstants::COMPLETED, OrderConstants::CANCELLED])->default(OrderConstants::PENDING);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
