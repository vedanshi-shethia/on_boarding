<?php

namespace App\Features\Orders\Http\Controllers\Admin\V1\Actions;

use App\Features\Orders\Domain\Imports\OrdersImport;
use App\Jobs\OrdersImportJob;
use Maatwebsite\Excel\Facades\Excel;


class OrdersImportAction
{
    public function importOrders(array $orderData, $file) {
        OrdersImportJob::dispatch($orderData, $file);
    }
}
