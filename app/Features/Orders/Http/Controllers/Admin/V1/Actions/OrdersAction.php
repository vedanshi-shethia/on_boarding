<?php

namespace App\Features\Orders\Http\Controllers\Admin\V1\Actions;

use App\Features\Categories\Domain\Models\Category;
use App\Features\ItemOrder\Domain\Models\ItemOrder;
use App\Features\ItemOrder\Http\Controllers\Admin\V1\Actions\ItemOrdersAction;
use App\Features\Orders\Domain\Models\Order;
use App\Jobs\CreateOrderMailJob;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class OrdersAction
{
    private ItemOrdersAction $itemOrdersAction;

    public function __construct() {
        $this->itemOrdersAction = new ItemOrdersAction();
    }
    public function persistOrder(array $data) : Order{
        return DB::transaction(function () use($data){
            $order = Order::persistOrder($data['order']);
            $net_total = 0;
            foreach($data['item_order'] as $requestItemOrder) {
                $net_total += ItemOrder::persistItemOrder($requestItemOrder, $order);
            }
            $order->net_total = $net_total;
            $order->save();
            CreateOrderMailJob::dispatch($order->customer_name);
            return $order;
        });
    }

    public function updateOrderWithOrderItem(array $orderData, Order $order, array $ItemOrderData, Collection $itemOrders) {
        DB::transaction(function () use($orderData, $order, $ItemOrderData, $itemOrders){
            $this->updateOrder($orderData, $order);
            $this->itemOrdersAction->updateItemOrders($ItemOrderData, $order, $itemOrders);
        });
    }

    public function getProducts($categoryId) {
        return Category::where('id', $categoryId)->firstOrFail()->products()->isActive()->get();
    }

    public function updateOrder(array $data, Order $order) {
        return $order->updateOrder($data);
    }

    public function cancelOrder(Order $order) : Order{
        return $order->cancelOrder();
    }

    public function getItemOrdersWithProduct(Order $order) : Collection{
        return $order->products()->withPivot('order_id', 'product_id', 'quantity', 'unit_price')->get();
    }

    public function getItemOrdersWithProductAndCategory(Order $order) : Collection {
        return $order->products()->withPivot('order_id', 'product_id', 'quantity', 'unit_price')->with('category')->get();
    }

    public function getActiveCategories() :Collection{
        return Category::getActiveCategories();
    }

    public function getItemOrder(int $orderId) : Collection {
        return ItemOrder::where('order_id', $orderId)->get();
    }

}
