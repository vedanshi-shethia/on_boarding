<?php

namespace App\Features\Orders\Http\Controllers\Admin\V1\Actions;

use App\Features\Orders\Domain\Exports\OrdersExport;
use App\Features\Orders\Domain\Exports\OrdersSkeletonExport;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Products\Domain\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Facades\Excel;
use function Sodium\add;

class OrdersExportAction
{
    public function exportOrders(array $data) {
        $query = Order::query();
        foreach($data as $attribute=> $value) {
            $query = $query->where($attribute,'like','%'. $value.'%');
        }
        $orders = $query->get();
        $itemOrders = new Collection();
        foreach($orders as $order){
            foreach ($order->products as $product) {
                $itemOrderData['order_id'] = $product->pivot->order_id;
                $itemOrderData['product_name'] = $product->name;
                $itemOrderData['quantity'] = $product->pivot->quantity;
                $itemOrderData['unit_price'] = $product->pivot->unit_price;
//                $product->pivote['product_name'] = $product->name;
                $itemOrders->push($itemOrderData);
//                dd($product->pivot);
            }
        }
//        dd($itemOrders);
        return Excel::download(new OrdersExport($orders, $itemOrders), 'orders.xlsx');
    }

    public function exportSkeleton() {
        $products = Product::isActive()->with('category')->get();
//        dd($products[0]->category->name);
        $productData = new Collection();
        foreach($products as $product) {
            $data['category_name'] = $product->category->name;
            $data['product_id'] = $product->id;
            $data['product_name'] = $product->name;
            $productData->push($data);
        }
//        dd($productData);
        return Excel::download(new OrdersSkeletonExport($productData), 'importFileFormat.xlsx');
    }
}
