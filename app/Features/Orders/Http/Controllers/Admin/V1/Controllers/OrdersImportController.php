<?php

namespace App\Features\Orders\Http\Controllers\Admin\V1\Controllers;

use App\Features\Orders\Http\Controllers\Admin\V1\Actions\OrdersImportAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Orders\ImportOrdersRequest;
use Illuminate\Support\Facades\Log;

class OrdersImportController extends Controller
{
    public function __construct(private OrdersImportAction $ordersImportAction){}
    public function import (ImportOrdersRequest $request){
//        dd($request->customer_email);
        $file = request()->file('item_order');
        $storedFile = $file->storeAs(
            "temp/orders/import-files",
            "temp".uniqid().".".$file->getClientOriginalName()
        );
        if(! $file) {
            Log::error("File Not found");
            session()->flash("error", "File not found");
            return redirect(route("admin.students.index"));
        }

        $orderData['customer_name'] = $request->customer_name;
        $orderData['customer_email'] = $request->customer_email;
        $orderData['order_date'] = $request->order_date;
//        dd($orderData);
        $this->ordersImportAction->importOrders($orderData,$storedFile);
        session()->flash('success', 'Products import started successfully');
        return redirect()->back();
    }
}
