<?php

namespace App\Features\Orders\Http\Controllers\Admin\V1\Controllers;

use App\DataTables\OrdersDataTable;
use App\Features\ItemOrder\Http\Controllers\Admin\V1\Actions\ItemOrdersAction;
use App\Features\Orders\Domain\Models\Constants\OrderConstants;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Orders\Http\Controllers\Admin\V1\Actions\OrdersAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Orders\CreateOrderRequest;
use App\Http\Requests\Orders\UpdateOrderRequest;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class OrdersController extends Controller
{
    public function __construct(private OrdersAction $ordersAction, private ItemOrdersAction $itemOrdersAction) {}

    public function index(OrdersDataTable $dataTable) {
        return $dataTable->render('admin.orders.index');
    }
    public function show(Order $order) {
        $itemOrders = $this->ordersAction->getItemOrdersWithProduct($order);
        return view('admin.orders.show', compact(['itemOrders']));
    }
    public function create() {
        $categories = $this->ordersAction->getActiveCategories();
        return view('admin.orders.create', compact(['categories']));
    }
    public function products(Request $request) {
        return $this->ordersAction->getProducts($request->category_id);
    }

    public function store(CreateOrderRequest $request)
    {
        try {
            $data['order'] = $request->order;
            $data['item_order'] = $request->item_order;
            $this->ordersAction->persistOrder($data);
            session()->flash('success', 'Order Created successfully');
        }catch(\Exception $e) {
            Log::error($e->getMessage());
            session()->flash('error', 'Something went wrong!');
        }
        return redirect()->route('admin.orders.index');
    }

    public function edit(Order $order) {
        $categories = $this->ordersAction->getActiveCategories();
        $productsWithPivot = $this->ordersAction->getItemOrdersWithProductAndCategory($order);
        $itemOrders = new Collection();
        foreach ($productsWithPivot as $productWithPivot) {
            $pivot['order_id'] = $productWithPivot->pivot->order_id;
            $pivot['product_id'] = $productWithPivot->pivot->product_id;
            $pivot['product_name'] = $productWithPivot->name;
            $pivot['product_stock'] = $productWithPivot->stock;
            $pivot['quantity'] = $productWithPivot->pivot->quantity;
            $pivot['unit_price'] = $productWithPivot->pivot->unit_price;
            $pivot['category_id'] = $productWithPivot->category_id;
            $pivot['category_name'] = $productWithPivot->category->name;
            $itemOrders->push($pivot);
        }
        $orderStatuses = OrderConstants::STATUS;
        return view('admin.orders.edit', compact(['order', 'categories', 'itemOrders', 'orderStatuses']));
    }

    public function update(UpdateOrderRequest $request, Order $order){
        $itemOrders = $this->ordersAction->getItemOrder($order->id);
        $orderData['customer_name'] = $request->order['customer_name'];
        $orderData['customer_email'] = $request->order['customer_email'];
        $orderData['order_date'] = $request->order['order_date'];
        $orderData['order_status'] = $request->order['order_status'];
        $counter = 0;
        $data = [];
        foreach ($request->item_order as $itemOrder) {
            $data['item_order'][$counter]['order_id'] = $order->id;
            $data['item_order'][$counter]['product_id'] = $itemOrder['product_id'];
            $data['item_order'][$counter]['category_id'] = $itemOrder['category_id'];
            $data['item_order'][$counter]['quantity'] = $itemOrder['quantity'];
            if($itemOrder['old_quantity'] != null) {
                $oldQuantityValidation = $itemOrders->contains($itemOrders->where('product_id', $itemOrder['product_id'])->where('quantity', $itemOrder['old_quantity'])->first());
                if(!$oldQuantityValidation) {
                    throw ValidationException::withMessages([ "old_quantity" =>"Old Quantity Should not be Disturbed"]);
                }
            }
            $data['item_order'][$counter]['old_quantity'] = $itemOrder['old_quantity'];
            $counter++;
        }
        $this->ordersAction->updateOrderWithOrderItem($orderData, $order, $data, $itemOrders);
        session()->flash('success', 'Order Updated successfully');
        return redirect()->route('admin.orders.index');
    }
    public function cancel(Order $order) {
        $this->ordersAction->cancelOrder($order);
        return redirect()->back();
    }
}
