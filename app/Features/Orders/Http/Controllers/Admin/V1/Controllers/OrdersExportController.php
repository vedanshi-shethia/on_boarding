<?php

namespace App\Features\Orders\Http\Controllers\Admin\V1\Controllers;

use App\Features\Orders\Http\Controllers\Admin\V1\Actions\OrdersExportAction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OrdersExportController extends Controller
{

    public function __construct(private OrdersExportAction $ordersExportAction){}

    public function export(Request $request) {
        $data = [];
        $request->customer_name ? $data['customer_name'] = $request->customer_name : null;
        $request->customer_email ? $data['customer_email'] = $request->customer_email : null;
        $request->net_total ? $data['net_total'] = $request->net_total : null;
        $request->order_status ? $data['order_status'] = $request->order_status : null;
        return $this->ordersExportAction->exportOrders($data);
    }

    public function exportSkeleton() {
        return $this->ordersExportAction->exportSkeleton();
    }
}
