<?php

use Illuminate\Support\Facades\Route;

Route::resource('/orders', \App\Features\Orders\Http\Controllers\Admin\V1\Controllers\OrdersController::class)->except('edit');

Route::get("/orders/{order}/edit", [\App\Features\Orders\Http\Controllers\Admin\V1\Controllers\OrdersController::class, "edit"])->name('orders.edit')->middleware(\App\Http\Middleware\UpdatingValidOrders::class);

Route::post("/orders/{order}/cancel", [\App\Features\Orders\Http\Controllers\Admin\V1\Controllers\OrdersController::class, 'cancel'])->name('orders.cancel')->middleware(\App\Http\Middleware\UpdatingValidOrders::class);

Route::post("/orders/products", [\App\Features\Orders\Http\Controllers\Admin\V1\Controllers\OrdersController::class, "products"])->name('orders.products');

