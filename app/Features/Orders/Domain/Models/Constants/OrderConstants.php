<?php

namespace App\Features\Orders\Domain\Models\Constants;

use App\Features\Products\Domain\Models\Product;

interface OrderConstants
{
    const PENDING = 'PENDING';
    const COMPLETED = 'COMPLETED';
    const CANCELLED = 'CANCELLED';

    const CREATE_VALIDATION_RULES = [
        "customer_name" => "required",
        "customer_email" => "required|email",
        "order_date" => "required|date",
    ];

    const UPDATE_VALIDATION_RULES = [
        "customer_name" => "required",
        "customer_email" => "required|email",
        "order_date" => "required|date",
    ];

    const STATUS = [
        self::PENDING,
        self::CANCELLED,
        self::COMPLETED
    ];
}
