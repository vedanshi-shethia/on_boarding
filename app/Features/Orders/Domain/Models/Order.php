<?php

namespace App\Features\Orders\Domain\Models;

use App\Features\ItemOrder\Domain\Models\ItemOrder;
use App\Features\Orders\Domain\Models\Constants\OrderConstants;
use App\Features\Products\Domain\Models\Product;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Database\Factories\OrderFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends BaseModel implements OrderConstants
{
    protected static function newFactory()
    {
        return OrderFactory::new();
    }

    public function products() {
        return $this->belongsToMany(Product::class, "item_order", "order_id", "product_id")->withTimestamps();
    }

    public static function persistOrder(array $data) {
        Utils::validateOrThrow($data, self::CREATE_VALIDATION_RULES);
        return DB::transaction(function () use($data) {
            return Order::create($data);
        });
    }

    public function updateOrder(array $data)
    {
        Utils::validateOrThrow($data, self::UPDATE_VALIDATION_RULES);
        DB::transaction(function () use($data) {
            $this->update($data);
        });
        return $this;
    }

    public function cancelOrder()
    {
        $this->reStockOrderItems();
        $this->update(['order_status' => OrderConstants::CANCELLED]);
        return $this;
    }

    public function reStockOrderItems() {
        $itemOrders = $this->products()->withPivot('quantity')->get();
        foreach ($itemOrders as $itemOrder) {
            $stock = $itemOrder->stock + $itemOrder->pivot->quantity;
            $itemOrder->update(['stock' => $stock]);
        }
    }
}
