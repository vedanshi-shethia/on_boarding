<?php

namespace App\Features\Orders\Domain\Imports;

use App\Features\Orders\Domain\Exports\OrdersFailureReportExport;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Products\Domain\Models\Product;
use App\Jobs\OrdersFailureReportMailJob;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Facades\Excel;

class OrdersImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    private array $orderData;
    const ATTRIBUTES = [
        'product_id',
        'quantity',
        'unit_price'
    ];

    private array $errorBag;

    public function __construct(array $orderData) {
       $this->orderData = $orderData;
    }
    public function collection(Collection $orderItems)
    {
        $order = Order::create($this->orderData);
        $rowCount = 0;
        $orderItems = $orderItems->toArray();
        $netTotal =0;
        foreach ($orderItems as $orderItem) {
            $validator = Validator::make($orderItem, [
               'product_id' => [
                   "required",
                   function ($attribute, $value, $fail) {
                       $product = Product::where('id', $value)->isActive()->first();
                       $product ? null: $fail('Invalid Product given');
               }],
                "quantity" => [
                    "required",
                    "numeric",
                    function($attribute, $value, $fail) use ($orderItem) {
                        $product = Product::where('id', $orderItem['product_id'])->isActive()->first();
                        if($product) {
                            $value > $product->stock ? $fail('Quantity should be less than inStock'): null;
                        }
                    }
                ],
            ]);
            $errorBagData = $this->makeDataForReport($orderItem);
            foreach(self::ATTRIBUTES as $attribute) {
                $this->errorBag[$rowCount][$attribute] = $errorBagData[$attribute];
            }
            if($validator->fails()) {
                Log::error($validator->errors());
                $this->errorBag[$rowCount]['error'] = $validator->errors()->getMessageBag()->first();
//                dd($validator->errors());
            }else {
                $orderItemData = $this->makeData($orderItem);
                $product = Product::where('id', $orderItem['product_id'])->first();
                $order->products()->attach(["product_id"=>$product->id], $orderItemData);
                $product->stock -= $orderItem['quantity'];
                $product->save();
                $netTotal += $orderItemData['unit_price'] * $orderItemData['quantity'];
            }
            $rowCount++;
        }

        $order->net_total = $netTotal;
        $order->save();

        if($this->errorBag) {
            Excel::store(new OrdersFailureReportExport($this->errorBag), 'orders/errors.csv', 'public');
//            OrdersFailureReportMailJob::dispatch($order->customer_email);
        }
    }

    private function makeData($orderItem) {
        $product = Product::where('id', $orderItem['product_id'])->first();
        return [
                'quantity' => $orderItem['quantity'],
                'unit_price' => $product->price,
        ];
    }

    private function makeDataForReport($orderItem)
    {
        $product = Product::where('id', $orderItem['product_id'])->first();
        return [
            'product_id' => $orderItem['product_id'],
            'quantity' => $orderItem['quantity'],
            'unit_price' => $product ? $product->price : 0,
        ];
    }
}
