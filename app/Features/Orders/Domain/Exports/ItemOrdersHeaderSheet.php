<?php

namespace App\Features\Orders\Domain\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ItemOrdersHeaderSheet implements WithHeadings, ShouldAutoSize
{
   public function headings() : array
   {
       return [
           "Product Name",
           "Quantity",
           "Unit Price"
       ];
   }

}
