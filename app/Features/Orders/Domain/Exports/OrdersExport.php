<?php

namespace App\Features\Orders\Domain\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OrdersExport implements WithMultipleSheets
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private Collection $orders;
    private Collection $itemOrders;
    public function __construct(Collection $orders, Collection $itemOrder)
    {
        $this->orders = $orders;
        $this->itemOrders = $itemOrder;
    }

    public function sheets(): array
    {
        return [
            'orders' => new OrdersSheet($this->orders),
            'item orders' => new ItemOrdersSheet($this->itemOrders)
        ];
    }
}
