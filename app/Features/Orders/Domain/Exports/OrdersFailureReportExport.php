<?php

namespace App\Features\Orders\Domain\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrdersFailureReportExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private array $failures;
    public function __construct(array $failures)
    {
        $this->failures = $failures;
    }

    public function collection()
    {
        return collect($this->failures);
    }

    public function headings(): array
    {
        return [
            "Product Name",
            "Quantity",
            "Unit Price",
            "Errors"
        ];
    }
}
