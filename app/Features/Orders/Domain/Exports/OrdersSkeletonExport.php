<?php

namespace App\Features\Orders\Domain\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OrdersSkeletonExport implements WithMultipleSheets
{

    public function __construct(private Collection $productDetails){}

    public function sheets(): array
    {
        return  [
            new ItemOrdersHeaderSheet(),
            new ProductDetailsSheet($this->productDetails)
        ];
    }
}
