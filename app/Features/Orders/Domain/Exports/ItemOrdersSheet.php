<?php

namespace App\Features\Orders\Domain\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ItemOrdersSheet implements FromCollection, ShouldAutoSize, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    private Collection $orderItems;
    public function __construct(Collection $orderItems) {
//        dd($orderItems);
        $this->orderItems = $orderItems;
    }

    public function collection()
    {
        return $this->orderItems;
    }

    public function headings() : array {
        return [
            "Order Id",
            "Product Name",
            "Quantity",
            "Unit Price",
        ];
    }
}
