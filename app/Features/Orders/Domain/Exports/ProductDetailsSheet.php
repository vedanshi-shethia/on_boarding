<?php

namespace App\Features\Orders\Domain\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductDetailsSheet implements FromCollection, WithHeadings, ShouldAutoSize
{

    public function __construct(private Collection $prdoductDetails){}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->prdoductDetails;
    }

    public function headings(): array
    {
        return [
            "Category Name",
            "Product Id",
            "Product Name"
        ];
    }
}
