$(function () {
    $("#edit-category-form").validate({
        rules: {
            name: {
                required: true,
                minlength: 1,
                normalizer: function(value) {
                    return $.trim(value);
                }
            },
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            console.log(placement);
            if (placement) {
                $(placement).append(error)
            }
        },
    });
});

console.log("Inside");
