$(document).ready( function() {
    console.log("hi");
    $(function(){
        window.LaravelDataTables = window.LaravelDataTables||{};window.LaravelDataTables["categories-table"]=$("#categories-table").DataTable(
            {
                "serverSide":true,
                "processing":true,
                "layout": {
                    topEnd: null
                },
                "ajax":{
                    "url":"http:\/\/127.0.0.1:8000\/admin\/categories",
                    "type":"GET",
                    "data":function(data) {
                        for (var i = 0, len = data.columns.length; i < len; i++) {
                            if (!data.columns[i].search.value) delete data.columns[i].search;
                            if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                            if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                            if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                        }
                        data.is_active = $('#status_search').val()
                        delete data.search.regex;
                    }
                },
                "columns":[
                    {"data":"id","name":"id","title":"Id","orderable":true,"searchable":true},
                    {
                        "data":"name",
                        "name":"name",
                        "title":"Name",
                        "orderable":true,
                        "searchable":true,
                    },
                    {"data":"description","name":"description","title":"Description","orderable":true,"searchable":true},{"data":"is_active","name":"is_active","title":"Is Active","orderable":true,"searchable":true},{"data":"created_at","name":"created_at","title":"Created At","orderable":true,"searchable":true},{"data":"updated_at","name":"updated_at","title":"Updated At","orderable":true,"searchable":true},{"defaultContent":"","data":"action","name":"action","title":"Action","render":null,"orderable":false,"searchable":false}
                ],
                "order":[[0,"desc"]],
                "select":{"style":"single"},
                "columnDefs": [{ width: 150, targets: 6 }],
            });
        $('#search-category-table').on("click", function () {
            // console.log($('#name_search').val())
            console.log(window.LaravelDataTables["categories-table"]);
            window.LaravelDataTables["categories-table"].column(1)
                .search($('#name_search').val())
                .draw();

            window.LaravelDataTables["categories-table"].column(2)
                .search($('#description_search').val())
                .draw();

            window.LaravelDataTables["categories-table"].column(3)
                .search($('#status_search').val())
                .draw();
        })
    });
})
