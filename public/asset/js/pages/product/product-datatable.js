$(document).ready(function () {
    $(function(){window.LaravelDataTables=window.LaravelDataTables||{};window.LaravelDataTables["products-table"]=$("#products-table").DataTable(
        {
            "serverSide":true,
            "processing":true,
            "layout": {
                topEnd: null
            },
            "ajax":{"url":"http:\/\/127.0.0.1:8000\/admin\/products",
                "type":"GET",
                "data":function(data) {
                    data.category_id = $('#category_select').val()
                    data.name = $('#name_search').val()
                    data.description = $('#description_search').val()
                    data.is_active = $('#is_active_select').val()
                    data.price = $('#price_search').val()
                    data.stock = $('#stock_search').val()
                    data.created_at_from = $('#created_at_from_search').val()
                    data.created_at_to = $('#created_at_to_search').val()
                    data.updated_at_from = $('#updated_at_from_search').val()
                    data.updated_at_to = $('#updated_at_to_search').val()
                    delete data.search.regex;
                }
            },
            "columns":[
                {"data":"id","name":"id","title":"Id","orderable":true,"searchable":true},
                {"data":"category_id","name":"category_id","title":"Category","orderable":true,"searchable":true},
                {"data":"name","name":"name","title":"Name","orderable":true,"searchable":true},
                {"data":"description","name":"description","title":"Description","orderable":true,"searchable":true},
                {"data":"price","name":"price","title":"Price","orderable":true,"searchable":true},
                {"data":"stock","name":"stock","title":"Stock","orderable":true,"searchable":true},
                {"data":"is_active","name":"is_active","title":"Is Active","orderable":true,"searchable":false},
                {"data":"created_at","name":"created_at","title":"Created At","orderable":true,"searchable":true},
                {"data":"updated_at","name":"updated_at","title":"Updated At","orderable":true,"searchable":true},
                {"defaultContent":"","data":"action","name":"action","title":"Action","render":null,"orderable":false,"searchable":false}
            ],
            "order":[[0,"desc"]],
            "select":{"style":"single"},
            "columnDefs": [{ width: 130, targets: 9 }],
        });

        $('#search-products-table').on("click", function () {
            window.LaravelDataTables["products-table"].draw();
        })
    });
})
