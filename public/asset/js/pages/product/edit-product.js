$(function () {
    $("#edit-product-form").validate({
        rules: {
            category:{
                required: true
            },
            name: {
                required: true,
                minlength: 1,
                normalizer: function(value) {
                    return $.trim(value);
                }
            },
            description: {
                maxlength:255
            },
            price: {
                required: true,
                number: true,
                min: 0
            },
            stock : {
                required: true,
                number : true,
                min : 0
            },
            is_active : {
                required : true
            }
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            console.log(placement);
            if (placement) {
                $(placement).append(error)
            }
        },
    });
});

