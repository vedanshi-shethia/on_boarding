$productCount = ($('.old-product-count').length != 0)? $('.old-product-count').prop('value'): 1;

$.validator.addClassRules("price",{
    required : true,
    number : true,
    min : 0
});

$.validator.addClassRules("stock",{
    required : true,
    number : true,
    min : 0
});

$.validator.addClassRules("category",{
    required : true
});

$.validator.addClassRules("name",{
    required: true,
    minlength: 1,
    normalizer: function(value) {
        return $.trim(value);
    }
});

$("#create-products-form").validate({
    errorElement: 'div',
    errorPlacement: function(error, element) {
        console.log(element)
        console.log(error)
        var placement = $(element).data('error');
        console.log(placement);
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    },
})

