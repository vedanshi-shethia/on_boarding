$.validator.addClassRules("customer_name",{
    required : true,
    minlength: 1,
    normalizer: function(value) {
        return $.trim(value);
    }
});


$.validator.addClassRules("customer_email",{
    required : true,
    minlength: 1,
    normalizer: function(value) {
        return $.trim(value);
    }
});

$.validator.addClassRules("order_date",{
    required : true,
});

$.validator.addClassRules("category",{
    required : true,
});

$.validator.addClassRules("product",{
    required : true,
});

$.validator.addClassRules("quantity",{
    required : true,
    // min: 0
});


$("#create-order-form").validate({
    errorElement: 'div',
    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        // console.log(placement);
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    },
})
