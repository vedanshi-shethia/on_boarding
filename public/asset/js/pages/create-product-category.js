$productCount = ($('.old-product-count').length != 0)? $('.old-product-count').prop('value'): 1;
$.validator.addClassRules("category_name",{
    required : true,
    normalizer: function(value) {
        return $.trim(value);
    }
});

$.validator.addClassRules("price",{
    required : true,
    number : true,
    min : 0
});

$.validator.addClassRules("stock",{
    required : true,
    number : true,
    min : 0
});


$("#create-products-form").validate({
    errorElement: 'div',
    errorPlacement: function(error, element) {
        console.log(element)
        console.log(error)
        var placement = $(element).data('error');
        console.log(placement);
        if (placement) {
            $(placement).append(error)
        }
    },
})

