$(document).ready(function() {
    $(function(){
        window.LaravelDataTables=window.LaravelDataTables||{};window.LaravelDataTables["orders-table"]=$("#orders-table").DataTable(
            {
                "serverSide":true,
                "processing":true,
                "layout": {
                    topEnd: null
                },
                "ajax":{"url":"http:\/\/127.0.0.1:8000\/admin\/orders",
                    "type":"GET",
                    "data":function(data) {
                        data.customer_name = $('#customer_name_search').val();
                        data.customer_email = $('#customer_email_search').val()
                        data.net_total = $('#net_total_search').val();
                        data.order_status = $('#order_status_select').val()
                        data.order_date_from = $('#order_date_from').val()
                        data.order_date_to = $('#order_date_to').val()
                        data.created_at_from = $('#created_at_from_search').val()
                        data.created_at_to = $('#created_at_to_search').val()
                        data.updated_at_from = $('#updated_at_from_search').val()
                        data.updated_at_to = $('#updated_at_to_search').val()
                        delete data.search.regex;
                    }
                },
                "columns":
                    [{"data":"id","name":"id","title":"Id","orderable":true,"searchable":true},{"data":"customer_name","name":"customer_name","title":"Customer Name","orderable":true,"searchable":true},{"data":"customer_email","name":"customer_email","title":"Customer Email","orderable":true,"searchable":true},{"data":"net_total","name":"net_total","title":"Net Total","orderable":true,"searchable":true},{"data":"order_date","name":"order_date","title":"Order Date","orderable":true,"searchable":true},{"data":"order_status","name":"order_status","title":"Order Status","orderable":true,"searchable":true},{"data":"created_at","name":"created_at","title":"Created At","orderable":true,"searchable":true},{"data":"updated_at","name":"updated_at","title":"Updated At","orderable":true,"searchable":true},{"defaultContent":"","data":"action","name":"action","title":"Action","render":null,"orderable":false,"searchable":false}],
                "order":[[1,"desc"]],
                "select":{"style":"single"},
                "columnDefs": [{ width: 500, targets: 8 }],
            })
        ;});

    $('#search-orders-table').on("click", function () {
        window.LaravelDataTables["orders-table"].draw();
    })
})
