$.validator.addClassRules("customer_name",{
    required : true,
});

// console.log("hi")
$.validator.addClassRules("customer_email",{
    required : true,
});

$.validator.addClassRules("order_date",{
    required : true,
});

$.validator.addClassRules("category",{
    required : true,
});

$.validator.addClassRules("product",{
    required : true,
});

$.validator.addClassRules("quantity",{
    required : true,
});


$("#edit-order-form").validate({
    errorElement: 'div',
    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        // console.log(placement);
        if (placement) {
            $(placement).append(error)
        }
    },
})
