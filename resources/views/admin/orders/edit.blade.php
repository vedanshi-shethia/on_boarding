@extends('admin.layouts.app')
@section('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
@endsection
@section('content')
    <div class="page-wrapper">
        <!-- Page header -->
        <div class="page-header d-print-none">
            <div class="container-xl">
                <div class="row g-2 align-items-center">
                    <div class="col">
                        <h2 class="page-title">
                            Orders
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-body">
            <div class="container">
                <div class="row-cards row">
                    <div class="col-md-12">
                        <form method="POST" action="{{route('admin.orders.update', $order->id)}}" class="repeater card"
                              id="edit-order-form">
                            @csrf
                            @method('PUT')
                            <div class="card-header">
                                <h4 class="card-title"><strong>Update orders</strong></h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="mb-3 col-md-3">
                                        <label class="form-label required">Customer Name</label>
                                        <input type="text" class="form-control validate customer_name" name="order[customer_name]" placeholder="John"
                                               data-error=".customer_name_error" data-test-id="input-customer-name" value = "{{old('order.customer_name') ? old('order.customer_name') : $order->customer_name}}">
                                        <div class="customer_name_error text-red"
                                             data-test-id="input-customer-name-error"></div>
                                        @error('order.customer_name')
                                        <div class="text-red">{{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="mb-3 col-md-3">
                                        <label class="form-label required">Customer Email</label>
                                        <input type="email" class="form-control validate customer_email" name="order[customer_email]" placeholder="john@email.com" data-error=".customer_email_error" value = "{{old('order.customer_email') ? old('order.customer_email') : $order->customer_email}}">
                                        <div class="customer_email_error text-red"></div>
                                        @error('order.customer_email')
                                        <div class="text-red">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label">Datepicker</label>
                                        <input class="form-control mb-2 order_date" placeholder="Select a date" id="datepicker-default" value="{{old('order.order_date') ? old('order.order_date') : $order->order_date}}" name = "order[order_date]">
                                        @error('order.order_date')
                                        <div class="text-red">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="mb-3 col-md-3">
                                        <div class="form-label">Select Order Status</div>
                                        <select class="orders-status-select2 form-select" name="order[order_status]" data-test-id="select-category" data-error=".category_error">
                                            @foreach($orderStatuses as $orderStatus)
                                                <option value="{{$orderStatus}}" {{old('order.order_status') ? ((old('order.order_status') == $orderStatus) ? 'selected': ''): (($order->order_status == $orderStatus)? 'selected': '')}}>{{$orderStatus}}</option>
                                            @endforeach
                                        </select>
                                        <div class="category_error text-red" data-test-id="select-category-error"></div>
                                        @error('order.order_status')
                                        <div class="text-red">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div data-repeater-list="item_order">
                                        {{--                                        @dd($itemOrders[0])--}}
                                        @if(old() == null)
                                            @for($i = 0; $i < count($itemOrders); $i++)
                                                {{--                                            @dd($itemOrders[0]['item_order']->quantity)--}}
                                                <div class="row" data-repeater-item>
                                                    <div class="mb-3 col-md-3">
                                                        <div class="form-label">Category</div>
                                                        <select class="categories-select2 form-select" name="item_order[{{$i}}][category_id]" data-test-id="select-category" data-error=".category_error"
                                                                onchange="getProducts(this)">
                                                            <option value="">Select Category</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{$category->id}}" {{$itemOrders[$i]['category_id'] == $category->id ? 'selected': ''}}>{{$category->name}} </option>
                                                            @endforeach
                                                        </select>
                                                        <div class="category_error text-red" data-test-id="select-category-error"></div>
                                                    </div>
                                                    <div class="mb-3 col-md-3">
                                                        <div class="form-label">Products</div>
                                                        <select class="products-select2 form-select" name="item_order[{{$i}}][product_id]" data-error=".product_error" onchange="setProductInfo(this)">
                                                            <option value="{{$itemOrders[$i]['product_id']}}" data-price = "{{$itemOrders[$i]['unit_price']}}" data-stock = "{{$itemOrders[$i]['product_stock']}}">{{$itemOrders[$i]['product_name']}}  - InStock : {{$itemOrders[$i]['product_stock']}}</option>
                                                        </select>
                                                        <input type="hidden">
                                                        <div class="product_error text-red" data-test-id="select-product-error"></div>
                                                    </div>
                                                    <div class="mb-3 col-md-2">
                                                        <label class="form-label required">Quantity</label>
                                                        <input type="number" class="form-control validate quantity" name="item_order[{{$i}}][quantity]" data-error=".quantity_error" data-price = "{{$itemOrders[$i]['unit_price']}}" min="1" value="{{$itemOrders[$i]['quantity']}}" onkeyup="setTotalAmount(this)">
                                                        <input type="hidden" value = "{{$itemOrders[$i]['quantity']}}" name="item_order[{{$i}}][old_quantity]" >
                                                        <div class="quantity_error text-red"></div>
                                                    </div>
                                                    <div class="mb-3 col-md-2">
                                                        <label class="form-label required">Unit Price</label>
                                                        <input type="number" class="form-control validate unit_price" name="item_order[{{$i}}][unit_price]" data-error=".unit_price_error" value="{{$itemOrders[$i]['unit_price']}}">
                                                        <div class="unit_price_error text-red"></div>
                                                    </div>
                                                    <div class="mb-3 col-md-2">
                                                        <input data-repeater-delete type="button" value="Delete"
                                                               class="btn btn-danger mt-5"/>
                                                    </div>
                                                </div>
                                            @endfor
                                        @else
                                            @for($i = 0; $i < count(old('item_order')); $i++)
                                                {{--                                                                                            @dd(old('item_order'))--}}
                                                <div class="row" data-repeater-item>
                                                    <div class="mb-3 col-md-3">
                                                        <div class="form-label">Category</div>
                                                        <select class="categories-select2 form-select category" name="item_order[{{$i}}][category_id]" data-test-id="select-category" data-error=".category_error"
                                                                onchange="getProducts(this)">
                                                            <option value="">Select Category</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{$category->id}}" {{(array_key_exists('category_id', old('item_order')[$i])) ? (old('item_order')[$i]['category_id'] == $category->id) ? 'selected': '': ''}}>{{$category->name}} </option>
                                                            @endforeach
                                                        </select>
                                                        <div class="category_error text-red" data-test-id="select-category-error"></div>
                                                        @error('item_order.'.$i.'.category_id')
                                                            <div class="text-red" data-test-id="select-category-error">{{$message}}</div>
                                                        @enderror
                                                    </div>
                                                    <div class="mb-3 col-md-3">
                                                        <div class="form-label">Products</div>
                                                        <select class="products-select2 form-select" name="item_order[{{$i}}][product_id]" data-error=".product_error" onchange="setProductInfo(this)">
                                                        </select>
                                                        <div class="product_error text-red" data-test-id="select-product-error"></div>
                                                        @error('item_order.'.$i.'.product_id')
                                                        <div class="text-red" data-test-id="select-category-error">{{$message}}</div>
                                                        @enderror
                                                    </div>
                                                    <div class="mb-3 col-md-2">
                                                        <label class="form-label required">Quantity</label>
                                                        <input type="number" class="form-control validate quantity" name="item_order[{{$i}}][quantity]" data-error=".quantity_error" min="1" value="{{array_key_exists('quantity', old('item_order')[$i]) ? old('item_order')[$i]['quantity']: ''}}" onkeyup="setTotalAmount(this)">
                                                        <input type="hidden" value = "{{array_key_exists('old_quantity', old('item_order')[$i]) ? old('item_order')[$i]['old_quantity']: ''}}" name="item_order[{{$i}}][old_quantity]" >
                                                        <div class="quantity_error text-red"></div>
                                                        <div class="quantity_error text-red"></div>
                                                        @error('item_order.'.$i.'.quantity')
                                                        <div class="text-red" data-test-id="select-category-error">{{$message}}</div>
                                                        @enderror
                                                    </div>
                                                    <div class="mb-3 col-md-2">
                                                        <label class="form-label required">Unit Price</label>
                                                        <input type="number" disabled class="form-control validate unit_price" name="item_order[{{$i}}][unit_price]" data-error=".unit_price_error" value="">
                                                        <div class="unit_price_error text-red"></div>
                                                        @error('item_order.'.$i.'.unit_price')
                                                        <div class="text-red" data-test-id="select-category-error">{{$message}}</div>
                                                        @enderror
                                                    </div>
                                                    <div class="mb-3 col-md-2">
                                                        <input data-repeater-delete type="button" value="Delete"
                                                               class="btn btn-danger mt-5"/>
                                                    </div>
                                                </div>
                                            @endfor
                                        @endif
                                    </div>
                                    <div class="mb-3 col-md-1">
                                        <input data-repeater-create type="button" value="Add" class="btn btn-primary"/>
                                    </div>
                                    <div class="mb-3 col-md-1">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                    <div class="mb-3 col-md-1">
                                        <label for="" id = "total_amount" class="btn bg-blue-lt">Total Amount : <strong>0</strong></label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{asset('vendor/repeater/jquery.repeater.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.js"></script>
{{--    <script src="{{asset('./asset/js/pages/edit-order.js')}}"></script>--}}

    <script>
        $(document).ready(function () {
            $('.categories-select2').select2({
                placeholder: "Select a Category",
                allowClear: true
            });
            $('.products-select2').select2({
                placeholder: "Select a Product",
                allowClear: true
            });

            $('.repeater').repeater({
                show: function () {
                    $(this).slideDown();
                    $('.products-select2').removeClass('select2-hidden-accessible');
                    $('.categories-select2').removeClass('select2-hidden-accessible');

                    $('.select2').remove();

                    $('.categories-select2').select2({
                        placeholder: "Select a Category",
                        allowClear: true
                    });
                    $('.products-select2').select2({
                        placeholder: "Select a Product",
                        allowClear: true
                    });

                    $('.select2-container').css('width', '100%');
                },
                isFirstItemUndeletable: true
            })
            setTotalAmount()
            @if(old())
                @for($i =0; $i < count(old('item_order')); $i++)
                @if(array_key_exists('product_id',old('item_order')[$i] ))
                $categoryElement = $("[name='item_order["+{{$i}}+"][category_id]'");
            $productElement = $("[name='item_order["+{{$i}}+"][product_id]'");
            $quantityInputElement = $("[name='item_order["+{{$i}}+"][quantity]'");
            $selectedProduct = parseInt("{{old('item_order')[$i]['product_id']}}");
            getProducts($categoryElement, $selectedProduct)
            setProductInfo($productElement)
            setTotalAmount($quantityInputElement)
            @endif
            @endfor
            @endif
        })

        function getProducts($categorySelect, $oldSelectedCategory = '') {
            $parentElement = $($categorySelect).parent().parent().children()
            $currentProduct = $($parentElement[1]).children()[1]
            $unitPriceInputElement = $($parentElement[3]).children()[1]
            $quantityInputElement = $($parentElement[2]).children()[1]
            $oldQuantityElement = $($parentElement[2]).children()[2]
            // console.log($currentProduct)

            $($oldQuantityElement).val('')
            // $($quantityInputElement).val('')
            $($unitPriceInputElement).val('')

            clearOldOptions()
            $categoryId = $($categorySelect).find('option:selected').val()
            // console.log($category)

            $.ajax({
                async: false,
                url: '{{route('admin.orders.products')}}',
                type: 'POST',
                data: {
                    category_id: $categoryId,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function (result) {
                    $.each(result, function (key, value) {
                        $selected = ''
                        @if(old('item_order'))
                        if($oldSelectedCategory === value.id){
                            $selected = 'selected';
                        }
                        @endif
                            console.log(value)
                        $($currentProduct).append(`<option value= '${value.id}' data-price = "${value.price}" data-stock = "${value.stock}" ${$selected}>${value.name}  - InStock : ${value.stock}</option>`);
                    });
                }
            });
        }

        function clearOldOptions() {
            $currentProduct.innerHTML = ''
            $($currentProduct).append('<option  value="" disabled selected>Select Product</option>')
        }

        function setProductInfo($productSelect){
            // console.log($productSelect)
            $parentElement = $($productSelect).parent().parent().children()
            $unitPriceInputElement = $($parentElement[3]).children()[1]
            $quantityInputElement = $($parentElement[2]).children()[1]
            $price = parseInt($($productSelect).find('option:selected').data("price"))
            $oldQuantityElement = $($parentElement[2]).children()[2]
            $($unitPriceInputElement).val($price)
            $($quantityInputElement).attr('data-price', $price)
            $inStock = parseInt($($productSelect).find('option:selected').data("stock"))
            // console.log($inStock)
            // $quantityInputValue = parseInt($($quantityInputElement).val())
            // if($inStock === 0) {
            //     $($quantityInputElement).attr("min", 0)
            //     $($quantityInputElement).val(0)
            // } else if($quantityInputValue >  $inStock || $($quantityInputElement).val() === '' || $quantityInputValue === 0) {
            //     $($quantityInputElement).attr("min", 1)
            //     $($quantityInputElement).val(1)
            // }
            // console.log($('.quantity'))
            $allQuantityInputElement = $('.quantity')
            $totalAmount = 0
            for($i =0; $i < $allQuantityInputElement.length; $i++) {
                $price = parseInt($($allQuantityInputElement[$i]).data('price'))
                $quantity = parseInt($($allQuantityInputElement[$i]).val())
                $totalAmount += ($price * $quantity)
                // console.log($price)
                // console.log($quantity)
            }
            console.log($totalAmount)
            if($totalAmount) {
                $('#total_amount').find('strong')[0].innerHTML = $totalAmount;
            }
        }

        function setTotalAmount($quantityInputElement) {
            console.log("hi")
            $parentElement = $($quantityInputElement).parent().parent().children()
            $productSelect = $($parentElement[1]).children()[1]
            $oldQuantityElement = $($parentElement[2]).children()[2]
            $inStock = parseInt($($productSelect).find('option:selected').data("stock"))
            console.log($inStock)
            console.log($($oldQuantityElement).val())
            // $quantity = parseInt($($quantityInputElement).val())
            // if($($oldQuantityElement).val()) {
            //     console.log(($($oldQuantityElement).val() - $($quantityInputElement).val()))
            //     if($inStock === 0) {
            //         $($quantityInputElement).val(0)
            //     }else if($($quantityInputElement).val() < 0 || ($($quantityInputElement).val() -$($oldQuantityElement).val()) > $inStock) {
            //         $($quantityInputElement).val(1)
            //     }
            // }else {
            //     if($inStock === 0) {
            //         $($quantityInputElement).val(0)
            //     }else if($($quantityInputElement).val() < 0 || $($quantityInputElement).val() > $inStock) {
            //         $($quantityInputElement).val(1)
            //     }
            // }

            $allQuantityInputElement = $('.quantity')
            $totalAmount = 0
            for($i =0; $i < $allQuantityInputElement.length; $i++) {
                $price = parseInt($($allQuantityInputElement[$i]).data('price'))
                $quantity = parseInt($($allQuantityInputElement[$i]).val())
                $totalAmount += ($price * $quantity)
                // console.log($price)
                // console.log($quantity)
            }
            console.log($totalAmount)
            if($totalAmount) {
                $('#total_amount').find('strong')[0].innerHTML = $totalAmount;
            }
        }
    </script>
    <script src="{{asset('asset/libs/litepicker/dist/litepicker.js')}}"></script>
    <script>
        // @formatter:off
        document.addEventListener("DOMContentLoaded", function () {
            window.Litepicker && (new Litepicker({
                    element: document.getElementById('datepicker-default'),
                    buttonText: {
                        previousMonth: `<!-- Download SVG icon from http://tabler-icons.io/i/chevron-left -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M15 6l-6 6l6 6" /></svg>`,
                        nextMonth: `<!-- Download SVG icon from http://tabler-icons.io/i/chevron-right -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M9 6l6 6l-6 6" /></svg>`,
                    },
                    minDate : new Date().setDate(new Date().getDate()),
                    startDate : new Date()
                })
            );
        });
        // @formatter:on
    </script>
@endsection
