@extends('admin.layouts.app')

@section('content')
    <!-- Page header -->
    <div class="page-header d-print-none">
        <div class="container-xl">
            <div class="row g-2 align-items-center mb-3">
                <div class="col">
                    <h2 class="page-title">
                        Orders
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="card-body m-3">
                    <div class="accordion" id="accordion-example">
                        <div class="accordion-item bg-white">
                            <h2 class="accordion-header" id="heading-1">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-1" aria-expanded="false">
                                    Advance Filtering and Import Export
                                </button>
                            </h2>
                            <div id="collapse-1" class="accordion-collapse collapse" data-bs-parent="#accordion-example" style="">
                                <div class="accordion-body pt-0">
                                    <form action="{{route('admin.orders.export')}}" method="POST" class="row" id="search_orders_from">
                                        @csrf
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Customer Name</label>
                                            <input type="text" class="form-control" name="customer_name" id="customer_name_search" placeholder="John">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Customer Email</label>
                                            <input type="text" class="form-control validate name" name="customer_email" id="customer_email_search" placeholder="john@gmail.com">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label ">Net Total</label>
                                            <input type="number" class="form-control" name="net_total" id="net_total_search" placeholder="₹0.0">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <div class="form-label">Select Status</div>
                                            <select class="form-select" name="order_status" id="order_status_select">
                                                <option value="" class="default_search">Select Status</option>
                                                <option>Pending</option>
                                                <option>Completed</option>
                                                <option>Canceled</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label">Order Date From</label>
                                            <input class="form-control mb-2" placeholder="Select a date" id="order_date_from" name = "order_date">
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label">Order Date To</label>
                                            <input class="form-control mb-2" placeholder="Select a date" id="order_date_to" name = "order_date">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Created At From</label>
                                            <input type="text" class="form-control" name="created_at" data-test-id="input-created-at" id="created_at_from_search" placeholder="Select a date">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Created At To</label>
                                            <input type="text" class="form-control" name="created_at" data-test-id="input-created-at" id="created_at_to_search" placeholder="Select a date">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Updated At From</label>
                                            <input type="text" class="form-control" name="updated_at" data-test-id="input-updated-at" id="updated_at_from_search" placeholder="Select a date">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Updated At To</label>
                                            <input type="text" class="form-control" name="updated_at" data-test-id="input-updated-at" id="updated_at_to_search" placeholder="Select a date">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1 m-1 me-2">
                                                <button type="button" class="btn bg-blue-lt" id="search-orders-table">Search <i class="ti ti-search"></i></button>
                                            </div>
                                            <div class="col-md-1 m-1">
                                                <button type = "submit" class="btn bg-blue-lt">Export <i class="ti ti-download"></i></button>
                                            </div>
                                            <div class="col-md-1 m-1">
                                                <a href="#" class="btn bg-blue-lt" data-bs-toggle="modal" data-bs-target="#modal-team">
                                                    Import <i class="ti ti-upload"></i>
                                                </a>
                                            </div>

                                            <div class="col-md-1 m-1">
                                                <button type = "button" class="btn bg-blue-lt" id="clear">
                                                    Clear <i class="ti ti-refresh"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        {{$dataTable->table()}}
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="modal-team" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Import Products</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mb-3 align-items-end">
                        <form action="{{route('admin.orders.exportSkeleton')}}" method="post" class="m-3">
                            @csrf
                            <div class="form-label">File format to import files</div>
                            <button type="submit" class="btn btn-primary">EXPORT SKELETON</button>
                        </form>

                        <form action="{{route('admin.orders.import')}}" method="post" enctype="multipart/form-data" class="" id="search_orders_form">
                            @csrf
                            <div class="row m-2">
                                <div class="col-md-6">
                                    <div class="form-label">Customer Name</div>
                                    <input type="text" class="form-control" name="customer_name" required>
                                    @error('customer_name')
                                        <div class="text-red">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <div class="form-label">Customer Email</div>
                                    <input type="email" class="form-control" name="customer_email" required>
                                    @error('customer_email')
                                    <div class="text-red">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12">
                                    <label class="form-label">Order Date</label>
                                    <input class="form-control mb-2" placeholder="Select a date" id="import_order_date" name = "order_date" required>
                                    @error('order_date')
                                    <div class="text-red">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="m-2">
                                <div class="form-label">Custom File Input</div>
                                <input type="file" class="form-control" name="item_order" required>
                                @error('item_order')
                                <div class="text-red">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="m-2">
                                <button type="submit" class="btn btn-primary mt-3">IMPORT</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="cancel-model" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="" method="post" id="cancel-order-form">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">Cancel Order</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to cancel order ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link link-secondary me-auto" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger" data-bs-dismiss="modal">Yes, cancel order</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@tabler/icons-webfont@latest/dist/tabler-icons.min.css" />
    <link rel="stylesheet" href="{{asset('vendor/datatables/dataTables.css')}}" />
@endsection

@section('scripts')
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/dataTables/dataTables.js')}}"></script>
    <script src="{{asset('asset/js/pages/order/order-datatable.js')}}"></script>
    <script src="{{asset('asset/libs/litepicker/dist/litepicker.js')}}"></script>
    <script src="{{asset('asset/js/pages/order/index-order.js')}}"></script>

@endsection
