@extends('admin.layouts.app')

@section('content')
{{--    {{dd($itemOrders)}}--}}
    <div class="page-header d-print-none">
        <div class="container-xl">
            <div class="row g-2 align-items-center mb-3">
                <div class="col">
                    <h2 class="page-title">
                        Item Order
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table card-table">
                           <thead>
                               <tr>
                                   <th>Product Name</th>
                                   <th>Product Quantity</th>
                                   <th>Unit Price</th>
                               </tr>
                           </thead>
                            <tbody>
                                @foreach($itemOrders as $itemOrder)
                                    {{--                            @dd($itemOrder)--}}
                                    <tr>
                                        <td>{{$itemOrder->name}}</td>
                                        <td>{{$itemOrder->pivot->quantity}}</td>
                                        <td>{{$itemOrder->price}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
