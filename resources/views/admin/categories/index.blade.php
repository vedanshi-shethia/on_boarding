@extends('admin.layouts.app')

@section('content')
    <!-- Page header -->
    <div class="page-header d-print-none">
        <div class="container-xl">
            <div class="row g-2 align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Categories
                    </h2>
                </div>

                <div class="card-body m-3">
                    <div class="row g-2 align-items-center">
                        <div class="mb-3 col-md-3">
                            <label class="form-label">Name</label>
                            <input type="text" class="form-control validate name" name="name" data-error=".name_error"
                                   data-test-id="input-name" id="name_search">
                        </div>
                        <div class="mb-3 col-md-3">
                            <label class="form-label">Description</label>
                            <input type="text" class="form-control validate name" name="description" data-error=".name_error"
                                   data-test-id="input-name" id="description_search">
                        </div>
                        <div class="mb-3 col-md-3">
                            <div class="form-label">Select</div>
                            <select class="form-select validate" name="" data-test-id = "select-status" data-error=".is_active_error0" id="status_search">
                                <option value="">Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <button type="button" class="btn bg-blue-lt" id="search-category-table">Search</button>
                            </div>
                        </div>
                    </div>
                    <div class="row g-2 align-items-center mt-3">
                        {{$dataTable->table()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="delete-model" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="" method="post" id="delete-category-form">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="modal-title">Delete category</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to Delete Category ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link link-secondary me-auto" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger" data-bs-dismiss="modal">Yes, delete all my data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@tabler/icons-webfont@latest/dist/tabler-icons.min.css" />
    <link rel="stylesheet" href="{{asset('vendor/datatables/dataTables.css')}}" />
@endsection

@section('scripts')
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/dataTables/dataTables.js')}}"></script>
    <script src="{{asset('asset/js/pages/category/index-category.js')}}"></script>
    <script src="{{asset('asset/js/pages/category/category-datatable.js')}}"></script>
@endsection
