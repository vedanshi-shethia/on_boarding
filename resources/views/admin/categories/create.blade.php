@extends('admin.layouts.app')

@section('content')
    <div class="page-wrapper">
        <!-- Page body -->
        <div class="page-body">
            <div class="container container-tight">
                <div class="row-cards row">
                    <div class="col-md-12">
                        <form method="POST" action="{{route('admin.categories.store')}}"  class="card card-md" id="create-category-form">
                            @csrf
                            <div class="create-categories">
                                <div class="card-header card-borderless">
                                    <h4 class="card-title"><strong>Create Categories</strong></h4>
                                </div>
                                <div class="card-body">
                                    <div class="row g-5">
                                        <div class="col-xl-12">
                                            <div class="row">
                                                <div class="col-md-6 col-xl-12">
                                                    <div class="mb-3">
                                                        <label class="form-label required">Name</label>
                                                        <input type="text" class="form-control validate" name="name" placeholder="Electronics" data-error =".name_error" value ="{{old("name")}}" data-test-id = "input-name">
                                                        <div class="name_error text-red" data-test-id = "input-name-error" ></div>
                                                        @error('name')
                                                        <div class="text-red" data-test-id = "input-name-error" >{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                    <div class="mb-3">
                                                        <label class="form-label">Description</label>
                                                        <textarea class="form-control validate" data-bs-toggle="autosize" placeholder="Some short description of category" name="description" data-error=".description_error" data-test-id = "input-description" >{{old("description")}}</textarea>
                                                        <div class="description_error text-red" data-test-id = "input-description-error"></div>
                                                        @error('description')
                                                        <div class="text-red" data-test-id = "input-description-error">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                    <div class="mb-3">
                                                        <div class="form-label" data-error=".is_active_error">Select</div>
                                                        <select class="form-select validate" name="is_active" data-test-id = "select-status">
                                                            <option {{old("is_active") == "1"? 'selected': ''}}  value="1">Active</option>
                                                            <option  {{old("is_active") == "0"? 'selected': ''}} value="0">Inactive</option>
                                                        </select>
                                                        <div class="is_active_error text-red" data-test-id = "select-status-error" ></div>
                                                        @error('is_active')
                                                        <div class="text-red" data-test-id = "select-status-error">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-end">
                                    <div class="d-flex">
                                        <button type="submit" class="btn btn-primary ">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/jquery/validation/jquery-validation.js')}}"></script>
    <script src="{{asset('./asset/js/pages/category/create-category.js')}}"></script>
@endsection
