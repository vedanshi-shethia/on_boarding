@extends('admin.layouts.app')

@section('content')
{{--    {{dd($category)}}--}}
    <div class="page-wrapper">
        <!-- Page header -->
        <div class="page-header d-print-none">
            <div class="container-xl">
                <div class="row g-2 align-items-center">
                    <div class="col">
                        <h2 class="page-title">
                            Cards
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page body -->
        <div class="page-body">
            <div class="container container-tight">
                <div class="row-cards row">
                    <div class="col-md-12">
                        <form method="POST" action="{{route('admin.products.update', $product)}}"  class="card card-md" id="edit-product-form">
                            @csrf
                            @method('PUT')
                            <div class="create-categories">
                                <div class="card-header card-borderless">
                                    <h4 class="card-title"><strong>Edit </strong></h4>
                                </div>
                                <div class="card-body">
                                    <div class="row g-5">
                                        <div class="row">
                                            <div class="mb-3 col-md-6">
                                                <div class="form-label">Category</div>
                                                <select class="form-select validate category"  name="category_id" data-test-id = "select-category" data-error=".category_error">
                                                    <option selected disabled>Select category</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}" {{array_key_exists("category_id", old()) ? (old("category_id") == $category->id? 'selected': ''):( $product->category_id === $category->id ? 'selected': '')}}>{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="category_error text-red" data-test-id = "select-category-error" ></div>
                                                @error('category')
                                                    <div class="text-red" data-test-id = "select-category-error">
                                                        {{$message}}</div>
                                                @enderror
                                            </div>
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label required">Name</label>
                                                <input type="text" class="form-control validate name" name="name" placeholder="Electronics" data-error =".name_error" data-test-id = "input-name" value = "{{old("name") ? old("name") : $product->name }}">
                                                <div class="name_error text-red" data-test-id = "input-name-error" ></div>
                                                @error('name')
                                                <div class="text-red" data-test-id = "select-name-error">
                                                    {{$message}}</div>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Description</label>
                                                <textarea class="form-control validate" data-bs-toggle="autosize" placeholder="Some short description of product" rows="4" name="description" data-error=".description_error" data-test-id = "input-description" >{{old("description") ? old("description") : $product->description }}</textarea>
                                                <div class="description_error text-red" data-test-id = "input-description-error"></div>
                                                @error('description')
                                                <div class="text-red" data-test-id = "select-description-error">
                                                    {{$message}}</div>
                                                @enderror
                                            </div>
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label required">Price</label>
                                                <input type="number" class="form-control validate price" name="price" placeholder="₹0.00" data-error =".price_error" data-test-id = "input-price" value = "{{old("price") ? old("price") : $product->price }}">
                                                <div class="price_error text-red" data-test-id = "input-price-error" ></div>
                                                @error('price')
                                                <div class="text-red" data-test-id = "select-price-error">
                                                    {{$message}}</div>
                                                @enderror
                                            </div>
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label required">Stock</label>
                                                <input type="number" class="form-control validate stock" name="stock" placeholder="0" data-error =".stock-error" data-test-id = "input-stock" value = "{{old("stock") ? old("stock") : $product->stock }}">
                                                <div class="stock-error text-red" data-test-id = "input-stock-error" ></div>
                                                @error('stock')
                                                <div class="text-red" data-test-id = "select-stock-error">
                                                    {{$message}}</div>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <div class="form-label">Select</div>
                                                <select class="form-select validate" name="is_active" data-test-id = "select-status" data-error=".is_active_error">
                                                    <option value="1" {{ array_key_exists("is_active", old()) ? (old("is_active") === "1"? 'selected': ''):( $product->is_active === 1 ? 'selected': '')}}>Active</option>
                                                    <option value="0" {{ array_key_exists("is_active", old()) ? (old("is_active") === "0"? 'selected': ''):( $product->is_active === 0 ? 'selected': '')}} >Inactive</option>
                                                </select>
                                                <div class="is_active_error text-red" data-test-id = "select-status-error" ></div>
                                                @error('is_active')
                                                <div class="text-red" data-test-id = "select-is_active-error">
                                                    {{$message}}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-end">
                                    <div class="d-flex">
                                        <button type="submit" class="btn btn-primary ">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.js"></script>
    <script src="{{asset('./asset/js/pages/product/edit-product.js')}}"></script>
@endsection

