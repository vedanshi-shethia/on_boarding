@extends('admin.layouts.app')

@section('content')
    <div class="page-wrapper">
        <!-- Page header -->
        <div class="page-header d-print-none">
            <div class="container-xl">
                <div class="row g-2 align-items-center">
                    <div class="col">
                        <h2 class="page-title">
                            Products
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page body -->
        <div class="page-body">
            <div class="container container-tight">
                <div class="row-cards row">
                    <div class="col-md-12">
                        <form method="POST" action="{{route('admin.products.store')}}"  class=" card" id="create-products-form">
                            @csrf
                            <div class="create-product">
                                <div class=" card-md">
                                    @if(old('product') === null)
                                        <div class="card-header card-borderless">
                                            <h4 class="card-title"><strong>Create Products</strong></h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="row g-5">
                                                <div class="row">
                                                    <div class="mb-3 col-md-6">
                                                        <div class="form-label">Category</div>
                                                        <select class="form-select validate category"  name="product[0][category_id]" data-test-id = "select-category" data-error=".category_error0">
                                                            <option  selected disabled>Select category</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="category_error0 text-red" data-test-id = "select-category-error" ></div>
                                                    </div>
                                                    <div class="mb-3 col-md-6">
                                                        <label class="form-label required">Name</label>
                                                        <input type="text" class="form-control validate name" name="product[0][name]" placeholder="Electronics" data-error =".name_error0" data-test-id = "input-name">
                                                        <div class="name_error0 text-red" data-test-id = "input-name-error" ></div>
                                                    </div>
                                                    <div class="mb-3">
                                                        <label class="form-label">Description</label>
                                                        <textarea class="form-control validate" data-bs-toggle="autosize" placeholder="Some short description of product" rows="4" name="product[0][description]" data-error=".description_error0" data-test-id = "input-description" ></textarea>
                                                        <div class="description_error0 text-red" data-test-id = "input-description-error"></div>
                                                    </div>
                                                    <div class="mb-3 col-md-6">
                                                        <label class="form-label required">Price</label>
                                                        <input type="number" class="form-control validate price" name="product[0][price]" placeholder="₹0.00" data-error =".price_error0" data-test-id = "input-price">
                                                        <div class="price_error0 text-red" data-test-id = "input-price-error" ></div>
                                                    </div>
                                                    <div class="mb-3 col-md-6">
                                                        <label class="form-label required">Stock</label>
                                                        <input type="number" class="form-control validate stock" name="product[0][stock]" placeholder="0" data-error =".stock-error0" data-test-id = "input-stock">
                                                        <div class="stock-error0 text-red" data-test-id = "input-stock-error" ></div>
                                                    </div>
                                                    <div class="mb-3">
                                                        <div class="form-label">Select</div>
                                                        <select class="form-select validate" name="product[0][is_active]" data-test-id = "select-status" data-error=".is_active_error0">
                                                            <option value="1">Active</option>
                                                            <option value="0">Inactive</option>
                                                        </select>
                                                        <div class="is_active_error0 text-red" data-test-id = "select-status-error" ></div>
                                                    </div>
                                                    <div>
                                                        <button type = "button" class="btn" onclick="addProductForm()">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-square-plus">
                                                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                                <path d="M9 12h6" />
                                                                <path d="M12 9v6" />
                                                                <path d="M3 5a2 2 0 0 1 2 -2h14a2 2 0 0 1 2 2v14a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2v-14z" />
                                                            </svg>Add More
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @elseif(old('product') !== null)
{{--                                        {{dd(old('product'))}}--}}
{{--                                        {!! $i = 0 !!}--}}
                                    <?php $i = 0; ?>
                                        @foreach(old('product') as $product)
                                            <div class="card-header card-borderless">
                                                <h4 class="card-title"><strong>Create Products</strong></h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="row g-5">
                                                    <div class="row">
                                                        <div class="mb-3 col-md-6">
                                                            <div class="form-label">Category</div>
                                                            <select class="form-select validate category" name="product[{{$i}}][category_id]" data-test-id = "select-category" data-error=".category_error{{$i}}">
                                                                <option  selected disabled>Select category</option>
                                                                @foreach($categories as $category)
                                                                    <option value="{{$category->id}}" {{(array_key_exists("category_id", $product) and ($product["category_id"] == $category->id)) ? 'selected':''}}>{{$category->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <div class="category_error{{$i}} text-red" data-test-id = "select-category-error" ></div>
                                                            @error('product.'.$i.'.category_id')
                                                            <div class="text-red" data-test-id = "select-category-error">{{ preg_replace("#product\\.[\d.]\\.category#", "category", $message)}}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="mb-3 col-md-6">
                                                            <label class="form-label required">Name</label>
                                                            <input type="text" class="form-control validate name" name="product[{{$i}}][name]" placeholder="Electronics" data-error =".name_error{{$i}}" value ='{{$product["name"]}}' data-test-id = "input-name">
                                                            <div class="name_error{{$i}} text-red" data-test-id = "input-name-error" ></div>
                                                            @error('product.'.$i.'.name')
                                                            <div class="text-red" data-test-id = "select-name-error">{{ preg_replace("#product\\.[\d.]\\.name#", "name", $message)}}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="mb-3">
                                                            <label class="form-label">Description</label>
                                                            <textarea class="form-control validate description" data-bs-toggle="autosize" placeholder="Some short description of product" rows="4" name="product[{{$i}}][description]" data-error=".description_error{{$i}}" data-test-id = "input-description" >{{$product["description"]}}</textarea>
                                                            <div class="description_error{{$i}} text-red" data-test-id = "input-description-error"></div>
                                                            @error('product.'.$i.'.description')
                                                            <div class="text-red" data-test-id = "select-description-error">{{ preg_replace("#product\\.[\d.]\\.category#", "description", $message)}}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="mb-3 col-md-6">
                                                            <label class="form-label required">Price</label>
                                                            <input type="number" class="form-control validate price" name="product[{{$i}}][price]" placeholder="₹0.00" data-error =".price_error{{$i}}" value ='{{$product["price"]}}' data-test-id = "input-price">
                                                            <div class="price_error{{$i}} text-red" data-test-id = "input-price-error" ></div>
                                                            @error('product.'.$i.'.price')
                                                            <div class="text-red" data-test-id = "select-price-error">{{ preg_replace("#product\\.[\d.]\\.price#", "price", $message)}}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="mb-3 col-md-6">
                                                            <label class="form-label required">Stock</label>
                                                            <input type="number" class="form-control validate stock" name="product[{{$i}}][stock]" placeholder="0" data-error =".stock_error{{$i}}" value ="{{$product["stock"]}}" data-test-id = "input-price">
                                                            <div class="stock_error{{$i}} text-red" data-test-id = "input-stock-error" ></div>
                                                            @error('product.'.$i.'.stock')
                                                            <div class="text-red" data-test-id = "select-stock-error">{{ preg_replace("#product\\.[\d.]\\.stock#", "stock", $message)}}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="mb-3">
                                                            <div class="form-label">Select</div>
                                                            <select class="form-select validate is_active" name="product[{{$i}}][is_active]" data-test-id = "select-status" data-error=".is_active_error{{$i}}">
                                                                <option {{$product["is_active"] == "1"? 'selected': ''}}  value="1" >Active</option>
                                                                <option  {{$product["is_active"] == "0"? 'selected': ''}} value="0">Inactive</option>
                                                            </select>
                                                            <div class="is_active_error{{$i}} text-red" data-test-id = "select-status-error" ></div>
                                                            @error('product.'.$i.'.is_active')
                                                            <div class="text-red" data-test-id = "select-is_active-error">{{ preg_replace("#product\\.[\d.]\\.is_active#", "is_active", $message)}}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="">
                                                            <button type = "button" class="btn m-lg-2" onclick="addProductForm()">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-square-plus">
                                                                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                                    <path d="M9 12h6" />
                                                                    <path d="M12 9v6" />
                                                                    <path d="M3 5a2 2 0 0 1 2 -2h14a2 2 0 0 1 2 2v14a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2v-14z" />
                                                                </svg>Add More
                                                            </button>
                                                            @if($i !== 0)
                                                                <button type="button" class="btn btn-outline-danger" onclick="removeProductForm(this)">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-trash">
                                                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                                        <path d="M4 7l16 0" />
                                                                        <path d="M10 11l0 6" />
                                                                        <path d="M14 11l0 6" />
                                                                        <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
                                                                        <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
                                                                    </svg>Delete
                                                                </button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                <?php $i++; ?>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            @if(old('product') === null)
                                <div class="card-footer text-end">
                                    <div class="d-flex">
                                        <button type="submit" class="btn btn-primary ">Submit</button>
                                    </div>
                                </div>
                            @else
                                <div class="card-footer text-end">
                                    <div class="d-flex">
                                        <button type="submit" class="btn btn-primary ">Submit</button>
                                    </div>
                                    <input type="hidden" value = "{{count(old('product'))}}" class = "old-product-count">
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.js"></script>
    <script src="{{asset("./asset/js/pages/product/create-product.js")}}"></script>
    <script>
        $productCount = ($('.old-product-count').length != 0)? $('.old-product-count').prop('value'): 1;
        function addProductForm() {
            $('.create-product').append(createProductForm($productCount))
            $productCount++
        }

        function removeProductForm($element){
            console.log($($element).parent().parent().parent().parent().parent().parent().remove())
            $productCount--
        }

        function createProductForm($productCount) {
            return `
    <div class="card-md">
          <div class="card-header card-borderless">
                <h4 class="card-title"><strong>Create Products</strong></h4>
          </div>
          <div class="card-body">
                                    <div class="row g-5">
                                        <div class="">
                                            <div class="row">
                                                    <div class="mb-3 col-md-6">
                                                        <div class="form-label" data-error=".category_error${$productCount}">Category</div>
                                                        <select class="form-select validate category" name="product[${$productCount}][category_id]" data-test-id = "select-category" data-error=".category_error${$productCount}">
                                                            <option  selected disabled>Select category</option>
                                                                @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
                                                                @endforeach
            </select>
            <div class="category_error${$productCount} text-red" data-test-id = "select-category-error" ></div>
        </div>
        <div class="mb-3 col-md-6">
            <label class="form-label required">Name</label>
            <input type="text" class="form-control validate name" name="product[${$productCount}][name]" placeholder="Electronics" data-error =".name_error${$productCount}" value ='' data-test-id = "input-name">
                                                        <div class="name_error${$productCount} text-red" data-test-id = "input-name-error" ></div>
                                                    </div>
                                                    <div class="mb-3">
                                                        <label class="form-label">Description</label>
                                                        <textarea class="form-control validate" data-bs-toggle="autosize" placeholder="Some short description of product" rows="4" name="product[${$productCount}][description]" data-error=".description_error${$productCount}" data-test-id = "input-description" ></textarea>
                                                        <div class="description_error${$productCount} text-red" data-test-id = "input-description-error"></div>
                                                    </div>
                                                    <div class="mb-3 col-md-6">
                                                        <label class="form-label required">Price</label>
                                                        <input type="number" class="form-control validate price" name="product[${$productCount}][price]" placeholder="₹0.00" data-error =".price_error${$productCount}" value ='' data-test-id = "input-price">
                                                        <div class="price_error${$productCount} text-red" data-test-id = "input-price-error" ></div>
                                                    </div>
                                                    <div class="mb-3 col-md-6">
                                                        <label class="form-label required">Stock</label>
                                                        <input type="number" class="form-control validate stock" name="product[${$productCount}][stock]" placeholder="0" data-error =".stock_error${$productCount}" value ="" data-test-id = "input-stock">
                                                        <div class="stock_error${$productCount} text-red" data-test-id = "input-stock-error" ></div>
                                                    </div>
                                                    <div class="mb-3">
                                                        <div class="form-label" data-error=".is_active_error${$productCount}">Select</div>
                                                        <select class="form-select validate" name="product[${$productCount}][is_active]" data-test-id = "select-status">
                                                            <option value="1">Active</option>
                                                            <option value="0">Inactive</option>
                                                        </select>
                                                        <div class="is_active_error${$productCount} text-red" data-test-id = "select-status-error" ></div>
                                                    </div>
                                                    <div class="">
                                                        <button type = "button" class="btn m-lg-2" onclick="addProductForm()">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-square-plus">
                                                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                                <path d="M9 12h6" />
                                                                <path d="M12 9v6" />
                                                                <path d="M3 5a2 2 0 0 1 2 -2h14a2 2 0 0 1 2 2v14a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2v-14z" />
                                                            </svg>Add More
                                                        </button>
                                                        <button type="button" class="btn btn-outline-danger" onclick="removeProductForm(this)">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-trash">
                                                          <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                          <path d="M4 7l16 0" />
                                                          <path d="M10 11l0 6" />
                                                          <path d="M14 11l0 6" />
                                                          <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
                                                          <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
                                                        </svg>Delete
                                                        </button>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`
        }
    </script>
@endsection
