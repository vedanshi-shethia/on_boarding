@extends('admin.layouts.app')

@section('content')
    <!-- Page header -->
    <div class="page-header d-print-none">
        <div class="container-xl">
            <div class="row g-2 align-items-center mb-3">
                <div class="col">
                    <h2 class="page-title">
                        Products
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="card-body m-3">
                    <div class="accordion bg-white" id="accordion-example">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading-1">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-1" aria-expanded="false">
                                     Advance Filtering and Export and Import
                                </button>
                            </h2>
                            <div id="collapse-1" class="accordion-collapse collapse" data-bs-parent="#accordion-example" style="">
                                <div class="accordion-body pt-0">
                                    <form action="{{route('admin.products.export')}}" method="POST" class="row" id="search_product_form">
                                        @csrf
                                        <div class="mb-3 col-md-3">
                                            <div class="form-label">Select Category</div>
                                            <select class="form-select validate" name="category_id" id="category_select">
                                                <option value="" class="default_select">Select Category</option>
                                                    @for($i =0; $i < count($categories) ;$i++)
                                                        <option value="{{$categories[$i]->id}}">{{$categories[$i]->name}}</option>
                                                    @endfor
                                            </select>
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Name</label>
                                            <input type="text" class="form-control" name="name" id="name_search" placeholder="John">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Description</label>
                                            <input type="text" class="form-control" name="description" data-error=".name_error" data-test-id="input-name" id="description_search" placeholder="john@gmail.com">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <div class="form-label">Select</div>
                                            <select class="form-select" name="is_active" id="is_active_select">
                                                <option value="" class="default_search">Select Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label ">Price</label>
                                            <input type="number" class="form-control price" name="price" data-test-id="input-price" id="price_search" placeholder="₹0.0">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Stock</label>
                                            <input type="number" class="form-control price" name="stock" data-test-id="input-stock" id="stock_search" placeholder="₹0.0">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Created At From</label>
                                            <input type="text" class="form-control" name="created_at" data-test-id="input-created-at" id="created_at_from_search" placeholder="Select a date">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Created At To</label>
                                            <input type="text" class="form-control" name="created_at" data-test-id="input-created-at" id="created_at_to_search" placeholder="Select a date">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Updated At From</label>
                                            <input type="text" class="form-control" name="updated_at" data-test-id="input-updated-at" id="updated_at_from_search" placeholder="Select a date">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label">Updated At To</label>
                                            <input type="text" class="form-control" name="updated_at" data-test-id="input-updated-at" id="updated_at_to_search" placeholder="Select a date">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1">
                                                <button type="button" class="btn bg-blue-lt" id="search-products-table">Search</button>
                                            </div>
                                            <div class="col-md-1">
                                                <button type = "submit" class="btn bg-blue-lt">Export</button>
                                            </div>
                                            <div class="col-md-1">
                                                <a href="#" class="btn bg-blue-lt" data-bs-toggle="modal" data-bs-target="#modal-team">
                                                    Import
                                                </a>
                                            </div>
                                            <div class="col-md-1">
                                                <button type = "button" class="btn bg-blue-lt" id="clear">Clear</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        {{$dataTable->table()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-blur fade" id="delete-model" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete category</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to Delete Category ?</p>
                    <form action="" method="post" id="delete-order-form">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Yes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="modal-team" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Import Products</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mb-3 align-items-end">
                        <form action="{{route('admin.products.exportSkeleton')}}" method="post" class="m-3">
                            @csrf
                            <div class="form-label">File format to import files</div>
                            <button type="submit" class="btn btn-primary">EXPORT SKELETON</button>
                        </form>

                        <form action="{{route('admin.products.import')}}" method="post" enctype="multipart/form-data" class="">
                            @csrf
                            <div class="m-2">
                                <div class="form-label">Custom File Input</div>
                                <input type="file" class="form-control" name="products" required>
                            </div>
                            <div class="m-2">
                                <button type="submit" class="btn btn-primary mt-3">IMPORT</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@tabler/icons-webfont@latest/dist/tabler-icons.min.css" />
    <link rel="stylesheet" href="{{asset('vendor/datatables/dataTables.css')}}" />
@endsection

@section('scripts')
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/dataTables/dataTables.js')}}"></script>
    <script src="{{asset('asset/js/pages/product/product-datatable.js')}}"></script>
    <script src="{{asset('asset/libs/litepicker/dist/litepicker.js')}}"></script>
    <script src="{{asset('asset/js/pages/product/index-product.js')}}"></script>

@endsection
