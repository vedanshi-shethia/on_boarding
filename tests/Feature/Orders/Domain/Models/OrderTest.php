<?php

namespace Orders\Domain\Models;

use App\Features\Orders\Domain\Models\Order;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic feature test example.
     */

    public function testPersistMethodMustReturnOrderInstanceIfValidationPassedSuccessfully(){

        $data = [
            'customer_name' => 'Vedanshi',
            'customer_email' => 'ved17@gmail.com',
            'order_date' => Carbon::now()
        ];

        $order = Order::persistOrder($data);

        $this->assertInstanceOf(Order::class, $order);
        $this->assertEquals("Vedanshi", $order->getOriginal("customer_name"));
        $this->assertDatabaseCount("orders", 1);
        $this->assertDatabaseHas("orders", ["id" => 1]);
    }

    public function testPersistMethodShouldValidateData() {
        $data = [
            'customer_name' => 'Vedanshi',
            'order_date' => Carbon::now()
        ];

        $this->expectException(ValidationException::class);
        $order = Order::persistOrder($data);
    }

    public function testUpdateMethodOrderMustReturnStudentInstanceIfValidationPassedSuccessfully() {


        $order = Order::persistOrder([
            'customer_name' => 'Vedanshi',
            'customer_email' => 'ved17@gmail.com',
            'order_date' => Carbon::now()
        ]);

        $data = [
            'customer_name' => 'Rudra',
            'customer_email' => 'ved17@gmail.com',
            'order_date' => Carbon::now()
        ];

        $order = $order->updateOrder($data);

        $this->assertInstanceOf(Order::class, $order);
        $this->assertEquals("Rudra", $order->getOriginal("customer_name"));
        $this->assertDatabaseCount("orders", 1);
        $this->assertDatabaseHas("orders", ["id" => 1]);
    }

    public function testUpdateMethodShouldValidateData() {

        $order = Order::persistOrder([
            'customer_name' => 'Vedanshi',
            'customer_email' => 'ved17@gmail.com',
            'order_date' => Carbon::now()
        ]);

        $data = [
            'customer_name' => 'Rudra',
            'order_date' => Carbon::now()
        ];

        $this->expectException(ValidationException::class);

        $order = $order->updateOrder($data);
    }
}
