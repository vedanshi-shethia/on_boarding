<?php

namespace Category\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    use DatabaseMigrations;

    public function testPersistMethodMustReturnCategoryInstanceIfValidationPassedSuccessfully(): void {

        $data = [
            "name" => "Sports",
            "description" => "Athletic, String, etc..",
            "is_active" => true,
        ];

        $category = Category::persistCategory($data);

        $this->assertInstanceOf(Category::class, $category);
        $this->assertEquals("Sports", $category->getOriginal("name"));
        $this->assertDatabaseCount("categories", 1);
        $this->assertDatabaseHas("categories", ["id" => 1]);
    }

    public function testPersistMethodShouldValidateData() {
        $data = [
            "description" => "Athletic, String, etc..",
            "is_active" => true,
        ];

        $this->expectException(ValidationException::class);
        $category = Category::persistCategory($data);
    }

    public function testUpdateMethodCategoryMustReturnStudentInstanceIfValidationPassedSuccessfully()
    {
        $data = [
            "name" => "Sports",
            "description" => "Athletic, String, etc..",
            "is_active" => true,
        ];

        $category = Category::persistCategory($data);

        $data = [
            "name" => "Dance",
            "description" => "Athletic, String, etc..",
            "is_active" => true,
        ];

        $category = $category->updateCategory($data);

        $this->assertInstanceOf(Category::class, $category);
        $this->assertEquals("Dance", $category->getOriginal("name"));
        $this->assertDatabaseCount("categories", 1);
        $this->assertDatabaseHas("categories", ["id" => 1]);
    }

    public function testUpdateMethodShouldValidateData() {
        $data = [
            "name" => "Sports",
            "description" => "Athletic, String, etc..",
            "is_active" => true,
        ];

        $category = Category::persistCategory($data);

        $data = [
            "description" => "Athletic, String, etc..",
            "is_active" => true,
        ];

        $this->expectException(ValidationException::class);

        $category = $category->updateCategory($data);
    }

}
