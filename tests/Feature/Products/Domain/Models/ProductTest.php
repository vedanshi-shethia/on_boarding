<?php

namespace Product\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Exceptions\IsNotActiveException;
use App\Features\Products\Domain\Models\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    use DatabaseMigrations;

    public function testPersistMethodMustReturnProductInstanceIfValidationPassedSuccessfully(): void {
        $category = Category::persistCategory([
            "name" => "Sports",
            "description" => "category-1",
            "is_active" => true
        ]);

        $data = [
            "category_id" => $category->id,
            "name" => "wrist band",
            "description" => "Strong, etc..",
            "price" => 250,
            "stock" => 10,
            "is_active" => true,
        ];

        $product = Product::persistProduct($data);
//        dd($product);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals("wrist band", $product->getOriginal("name"));
        $this->assertDatabaseCount("products", 1);
        $this->assertDatabaseHas("products", ["id" => 1]);
    }

    public function testPersistMethodShouldValidateData() {
        $category = Category::persistCategory([
            "name" => "Sports",
            "description" => "category-1",
            "is_active" => true
        ]);

        $data = [
            "category_id" => $category->id,
            "description" => "Athletic, String, etc..",
            "is_active" => true,
        ];

        $this->expectException(ValidationException::class);
        $product = Product::persistProduct($data);

        $data["is_active"] = false;

        $this->expectException(IsNotActiveException::class);
        $product = Product::persistProduct($data);

    }

    public function testUpdateMethodProductMustReturnProductInstanceIfValidationPassedSuccessfully()
    {
        $category = Category::persistCategory([
            "name" => "Sports",
            "description" => "category-1",
            "is_active" => true
        ]);

        $data = [
            "category_id" => $category->id,
            "name" => "wrist band",
            "description" => "Strong, etc..",
            "price" => 250,
            "stock" => 10,
            "is_active" => true,
        ];

        $product = Product::persistProduct($data);

        $data = [
            "category_id" => $category->id,
            "name" => "wrist",
            "description" => "Strong, etc..",
            "price" => 250,
            "stock" => 10,
            "is_active" => true,
        ];

        $product = $product->updateProduct($data);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals("wrist", $product->getOriginal("name"));
        $this->assertDatabaseCount("products", 1);
        $this->assertDatabaseHas("products", ["id" => 1]);
    }

    public function testUpdateMethodShouldValidateData() {

        $category = Category::persistCategory([
            "name" => "Sports",
            "description" => "category-1",
            "is_active" => true
        ]);

        $data = [
            "category_id" => $category->id,
            "name" => "wrist band",
            "description" => "Strong, etc..",
            "price" => 250,
            "stock" => 10,
            "is_active" => true,
        ];

        $product = Product::persistProduct($data);

        $data = [
            "category_id" => $category->id,
            "description" => "Athletic, String, etc..",
            "is_active" => true,
        ];

        $this->expectException(ValidationException::class);

        $product = $product->updateProduct($data);
    }

}
