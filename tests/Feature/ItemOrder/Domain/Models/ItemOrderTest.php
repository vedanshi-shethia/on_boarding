<?php

namespace ItemOrder\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use App\Features\ItemOrder\Domain\Models\ItemOrder;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Products\Domain\Models\Product;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ItemOrderTest extends TestCase
{
    use DatabaseMigrations;

    public function testPersistMethodMustReturnTotalPriceIfValidationPassedSuccessfully() {
        $category = Category::persistCategory([
            "name" => "Sports",
            "description" => "category-1",
            "is_active" => true
        ]);
        $product = Product::persistProduct([
            "category_id" => $category->id,
            "name" => "wrist band",
            "description" => "Strong, etc..",
            "price" => 250,
            "stock" => 10,
            "is_active" => true,
        ]);

        $order = Order::persistOrder([
            'customer_name' => 'Vedanshi',
            'customer_email' => 'ved17@gmail.com',
            'order_date' => Carbon::now()
        ]);

        $data = [
            'category_id' => $category->id,
            'product_id' => $product->id,
            'quantity' => 2,
            'unit_price' => $product->price
        ];
        $actualNetTotal = $product->price * $data['quantity'];
        $netTotal = ItemOrder::persistItemOrder($data, $order);
        $this->assertIsNumeric($netTotal);
        $this->assertEquals($netTotal, $actualNetTotal);
        $this->assertDatabaseCount("item_order", 1);
        $this->assertDatabaseHas("item_order", ["id" => 1]);
    }

    public function testUpdateMethodOrderMustReturnNetTotalInstanceIfValidationPassedSuccessfully() {
        $category = Category::persistCategory([
            "name" => "Sports",
            "description" => "category-1",
            "is_active" => true
        ]);
        $product = Product::persistProduct([
            "category_id" => $category->id,
            "name" => "wrist band",
            "description" => "Strong, etc..",
            "price" => 250,
            "stock" => 10,
            "is_active" => true,
        ]);

        $order = Order::persistOrder([
            'customer_name' => 'Vedanshi',
            'customer_email' => 'ved17@gmail.com',
            'order_date' => Carbon::now()
        ]);

        $netTotal = ItemOrder::persistItemOrder([
            'category_id' => $category->id,
            'product_id' => $product->id,
            'quantity' => 2,
            'unit_price' => $product->price
        ], $order);

        $data = [
            'category_id' => $category->id,
            'product_id' => $product->id,
            'quantity' => 3,
            'unit_price' => $product->price,
            'old_quantity' => 2
        ];

        $itemOrder = ItemOrder::where('order_id', $order->id)->firstOrFail();

        $netTotal = $itemOrder->updateItemOrder($data, $order);
        $this->assertIsNumeric($netTotal);
        $this->assertDatabaseCount("item_order", 1);
        $this->assertDatabaseHas("item_order", ["quantity" => 3]);

    }
}
